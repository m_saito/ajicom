# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140728081918) do

  create_table "contents", :force => true do |t|
    t.string   "file_name"
    t.string   "content_type"
    t.string   "size"
    t.string   "file_url"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "global_searches", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "text"
  end

  create_table "mbrands", :force => true do |t|
    t.string   "brand_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "mbrands", ["brand_name"], :name => "index_mbrands_on_brand_name", :unique => true

  create_table "mcategories", :force => true do |t|
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "mchecks", :force => true do |t|
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "mcompanies", :force => true do |t|
    t.integer  "muser_id"
    t.string   "company_name"
    t.string   "company_kana"
    t.string   "represent_name"
    t.string   "represent_kana"
    t.string   "division"
    t.integer  "capital"
    t.datetime "establish_date"
    t.integer  "employees"
    t.string   "zip_code"
    t.string   "address"
    t.string   "station"
    t.string   "tel_no_1"
    t.string   "tel_no_2"
    t.string   "fax_no_1"
    t.string   "fax_no_2"
    t.string   "web_url"
    t.text     "remarks"
    t.string   "account_opened"
    t.string   "contracted"
    t.string   "tworks"
    t.string   "texperts"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "company_type"
  end

  create_table "mcompanytypes", :force => true do |t|
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "mcompusers", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "mexpertises", :force => true do |t|
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "mgenerals", :force => true do |t|
    t.string   "type_code"
    t.string   "code"
    t.string   "name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "mtopicuses", :force => true do |t|
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "musers", :force => true do |t|
    t.string   "user_name"
    t.string   "mail"
    t.string   "user_div"
    t.string   "login"
    t.string   "password"
    t.string   "question"
    t.string   "answer"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "user_kana"
  end

  create_table "mworkareas", :force => true do |t|
    t.integer  "mworkgroup_id"
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "mworkareasubs", :force => true do |t|
    t.integer  "mworkarea_id"
    t.string   "name"
    t.string   "another_name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "mworkgroups", :force => true do |t|
    t.string   "name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tcategorybytopics", :force => true do |t|
    t.integer  "mcategory_id"
    t.integer  "ttopic_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "tcheckbytopics", :force => true do |t|
    t.integer  "mcheck_id"
    t.integer  "ttopic_id"
    t.string   "name"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "texpertbycomps", :force => true do |t|
    t.integer  "mcompany_id"
    t.integer  "mexpertise_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "tfilebytopics", :force => true do |t|
    t.integer  "ttopic_id"
    t.string   "content_type"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "upload_file_name"
    t.float    "upload_file_size"
    t.string   "upload_file"
    t.text     "upload_file_url"
    t.text     "thumbnail_url"
    t.text     "thumbnail2_url"
    t.string   "upload_file_no_image"
    t.boolean  "upload_file_no_image_process"
    t.string   "upload_file_no_image_tmp"
    t.boolean  "upload_file_process"
    t.string   "upload_tmp"
  end

  create_table "tstaffbycomps", :force => true do |t|
    t.integer  "mcompany_id"
    t.string   "name"
    t.string   "kana"
    t.string   "mail"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "ttopics", :force => true do |t|
    t.integer  "mcompany_id"
    t.string   "topic_type"
    t.string   "title"
    t.datetime "upload_date"
    t.text     "body_text"
    t.string   "client"
    t.string   "category"
    t.string   "brand"
    t.string   "tool"
    t.string   "web_url"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "upload_file_name"
    t.float    "upload_file_size"
    t.string   "upload_file"
    t.integer  "enabled"
    t.integer  "client_rep"
    t.string   "deployment_season"
    t.integer  "printing_company"
    t.integer  "production_cost"
    t.string   "develop_company"
    t.integer  "client_id"
    t.integer  "ajicomsales"
    t.integer  "cr_rep"
    t.string   "photo"
    t.string   "styling"
    t.date     "dep_season"
    t.string   "use"
  end

  create_table "tuserbycomps", :force => true do |t|
    t.integer  "mcompany_id"
    t.integer  "muser_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tworkbycomps", :force => true do |t|
    t.integer  "mcompany_id"
    t.integer  "mworkarea_id"
    t.integer  "mworkareasub_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.string   "role_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "auth_token"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "vdivbycomps", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
