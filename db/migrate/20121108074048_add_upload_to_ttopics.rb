class AddUploadToTtopics < ActiveRecord::Migration
  def change
    add_column :ttopics, :upload_file_name, :string
    add_column :ttopics, :upload_file_size, :float
  end
end
