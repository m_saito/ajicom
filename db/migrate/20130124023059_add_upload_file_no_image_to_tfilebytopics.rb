class AddUploadFileNoImageToTfilebytopics < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :upload_file_no_image, :string
  end
end
