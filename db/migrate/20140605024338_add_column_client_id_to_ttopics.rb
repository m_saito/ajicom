class AddColumnClientIdToTtopics < ActiveRecord::Migration
  def change
    add_column :ttopics, :client_id, :integer
  end
end
