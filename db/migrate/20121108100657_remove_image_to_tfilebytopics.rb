class RemoveImageToTfilebytopics < ActiveRecord::Migration
  def up
    remove_column :tfilebytopics, :image
    remove_column :tfilebytopics, :image_url
  end

  def down
    add_column :tfilebytopics, :image_url, :string
    add_column :tfilebytopics, :image, :string
  end
end
