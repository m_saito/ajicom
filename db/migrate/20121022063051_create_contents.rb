class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string    :file_name
      t.string    :content_type
      t.string    :size
      t.string    :file_url

      t.timestamps
    end
  end
end
