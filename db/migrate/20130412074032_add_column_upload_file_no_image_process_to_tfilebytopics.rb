class AddColumnUploadFileNoImageProcessToTfilebytopics < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :upload_file_no_image_process, :boolean
  end
end
