class CreateTexpertbycomps < ActiveRecord::Migration
  def change
    create_table :texpertbycomps do |t|
      t.integer     :mcompany_id
      t.integer     :mexpertise_id
      t.string      :created_by
      t.string      :updated_by
      t.timestamps
    end
  end
end
