class CreateTuserbycomps < ActiveRecord::Migration
  def change
    create_table :tuserbycomps do |t|
      t.integer     :mcompany_id
      t.integer     :muser_id
      t.string      :created_by
      t.string      :updated_by
      
      t.timestamps
    end
  end
end
