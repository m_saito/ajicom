class AddColumnAjicomsalesToTtopics < ActiveRecord::Migration
  def change
    add_column :ttopics, :ajicomsales, :integer
    add_column :ttopics, :cr_rep, :integer
    add_column :ttopics, :photo, :string
    add_column :ttopics, :styling, :string
  end
end
