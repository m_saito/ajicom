class CreateTstaffbycomps < ActiveRecord::Migration
  def change
    create_table :tstaffbycomps do |t|
      t.integer :mcompany_id
      t.string :name
      t.string :kana
      t.string :mail

      t.timestamps
    end
  end
end
