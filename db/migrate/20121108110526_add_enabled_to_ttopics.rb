class AddEnabledToTtopics < ActiveRecord::Migration
  def change
    add_column :ttopics, :enabled, :integer
  end
end
