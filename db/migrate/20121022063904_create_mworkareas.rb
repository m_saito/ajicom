class CreateMworkareas < ActiveRecord::Migration
  def change
    create_table :mworkareas do |t|
      t.integer     :mworkgroup_id
      t.string      :name
      t.string      :another_name
      t.string      :created_by
      t.string      :updated_by
      
      t.timestamps
    end
  end
end
