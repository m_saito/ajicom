class CreateMcompanytypes < ActiveRecord::Migration
  def change
    create_table :mcompanytypes do |t|
      t.string :id
      t.string :name
      t.string :another_name
      t.string :created_by
      t.string :updated_by

      t.timestamps
    end
  end
end
