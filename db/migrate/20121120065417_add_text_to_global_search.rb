class AddTextToGlobalSearch < ActiveRecord::Migration
  def change
    add_column :global_searches, :text, :string
  end
end
