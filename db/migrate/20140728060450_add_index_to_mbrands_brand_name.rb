class AddIndexToMbrandsBrandName < ActiveRecord::Migration
  def change
    add_index :mbrands, :brand_name, unique: true
  end
end
