class RemoveColumnDevelopCompanyToMcompanies < ActiveRecord::Migration
  def up
    remove_column :mcompanies, :develop_company
  end

  def down
    add_column :mcompanies, :develop_company, :string
  end
end
