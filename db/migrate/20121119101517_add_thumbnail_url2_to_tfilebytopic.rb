class AddThumbnailUrl2ToTfilebytopic < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :thumbnail2_url, :text
  end
end
