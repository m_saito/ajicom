class RemoveFileInfoToTtopics < ActiveRecord::Migration
  def up
  #  remove_column :ttopics, :upload_file_name
  #  remove_column :ttopics, :upload_file_size
  #  remove_column :ttopics, :file
  #  remove_column :ttopics, :upload_file
  end

  def down
    add_column :ttopics, :upload_file, :string
    add_column :ttopics, :file, :string
    add_column :ttopics, :upload_file_size, :string
    add_column :ttopics, :upload_file_name, :string
  end
end
