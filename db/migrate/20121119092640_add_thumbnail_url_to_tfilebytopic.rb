class AddThumbnailUrlToTfilebytopic < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :thumbnail_url, :text
  end
end
