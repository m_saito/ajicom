class AddColumnDepSeasonToTtopics < ActiveRecord::Migration
  def change
    add_column :ttopics, :dep_season, :date
  end
end
