class CreateMgenerals < ActiveRecord::Migration
  def change
    create_table :mgenerals do |t|
      t.string    :type_code
      t.string    :code
      t.string    :name
      t.string    :created_by
      t.string    :updated_by

      t.timestamps
    end
  end
end
