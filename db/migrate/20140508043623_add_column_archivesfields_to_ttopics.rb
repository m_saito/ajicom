class AddColumnArchivesfieldsToTtopics < ActiveRecord::Migration
  def change
    add_column :ttopics, :client_rep, :integer
    add_column :ttopics, :deployment_season, :string
    add_column :ttopics, :printing_company, :integer
    add_column :ttopics, :production_cost, :integer
  end
end
