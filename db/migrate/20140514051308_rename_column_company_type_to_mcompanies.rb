class RenameColumnCompanyTypeToMcompanies < ActiveRecord::Migration
  def up
    rename_column :mcompanies, :printing_company, :company_type
  end

  def down
    rename_column :mcompanies, :company_type, :printing_company
  end
end
