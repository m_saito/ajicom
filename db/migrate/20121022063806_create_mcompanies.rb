class CreateMcompanies < ActiveRecord::Migration
  def change
    create_table :mcompanies do |t|
      t.integer   :muser_id
      t.string    :company_name
      t.string    :company_kana
      t.string    :represent_name
      t.string    :represent_kana
      t.string    :division
      t.integer   :capital
      t.datetime  :establish_date
      t.integer   :employees
      t.string    :zip_code
      t.string    :address
      t.string    :station
      t.string    :tel_no_1
      t.string    :tel_no_2
      t.string    :fax_no_1
      t.string    :fax_no_2
      t.string    :web_url
      t.text      :remarks
      t.string    :account_opened
      t.string    :contracted
      t.string    :tworks
      t.string    :texperts     
      t.string    :created_by
      t.string    :updated_by 
      
      
      
      t.timestamps
    end
  end
end
