class CreateTworkbycomps < ActiveRecord::Migration
  def change
    create_table :tworkbycomps do |t|
      t.integer     :mcompany_id
      t.integer     :mworkarea_id
      t.integer     :mworkareasub_id
      t.string      :created_by
      t.string      :updated_by
      
      t.timestamps
    end
  end
end
