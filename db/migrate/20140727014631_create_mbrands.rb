class CreateMbrands < ActiveRecord::Migration
  def change
    create_table :mbrands do |t|
      t.string :brand_name

      t.timestamps
    end
  end
end
