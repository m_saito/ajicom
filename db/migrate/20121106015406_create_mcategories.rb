class CreateMcategories < ActiveRecord::Migration
  def change
    create_table :mcategories do |t|
      t.string :name
      t.string :another_name
      t.string :created_by
      t.string :updated_by

      t.timestamps
    end
  end
end
