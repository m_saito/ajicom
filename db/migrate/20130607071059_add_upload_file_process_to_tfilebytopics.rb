class AddUploadFileProcessToTfilebytopics < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :upload_file_process, :boolean
  end
end
