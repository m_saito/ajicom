class CreateMworkareasubs < ActiveRecord::Migration
  def change
    create_table :mworkareasubs do |t|
      t.integer     :mworkarea_id
      t.string      :name
      t.string      :another_name
      t.string      :created_by
      t.string      :updated_by
      
      t.timestamps
    end
  end
end
