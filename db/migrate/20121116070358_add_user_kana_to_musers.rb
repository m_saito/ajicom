class AddUserKanaToMusers < ActiveRecord::Migration
  def change
    add_column :musers, :user_kana, :string
  end
end
