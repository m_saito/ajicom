class RemoveFileInfoToTfilebytopics < ActiveRecord::Migration
  def up
    remove_column :tfilebytopics, :file_name
    remove_column :tfilebytopics, :data
  end

  def down
    add_column :tfilebytopics, :data, :binary
    add_column :tfilebytopics, :file_name, :string
  end
end
