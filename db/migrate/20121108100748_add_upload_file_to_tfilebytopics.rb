class AddUploadFileToTfilebytopics < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :upload_file, :string
    add_column :tfilebytopics, :upload_file_url, :text
  end
end
