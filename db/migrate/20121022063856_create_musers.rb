class CreateMusers < ActiveRecord::Migration
  def change
    create_table :musers do |t|
      t.string    :user_name
      t.string    :mail
      t.string    :user_div
      t.string    :login
      t.string    :password
      t.string    :question
      t.string    :answer
      t.string    :created_by
      t.string    :updated_by
      
      t.timestamps
    end
  end
end
