class AddUploadToTfilebytopic < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :upload_file_name, :string
    add_column :tfilebytopics, :upload_file_size, :float
  end
end
