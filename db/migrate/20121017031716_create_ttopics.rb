class CreateTtopics < ActiveRecord::Migration
  def change
    create_table :ttopics do |t|

      t.integer   :mcompany_id
      t.string    :topic_type
      t.string    :title
      t.datetime  :upload_date
      t.text      :body_text
      t.string    :client
      t.string    :category
      t.string    :brand
      t.string    :tool
      t.string    :web_url
      
      t.string    :created_by
      t.string    :updated_by
      
      t.timestamps
    end
  end
end
