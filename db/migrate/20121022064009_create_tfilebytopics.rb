class CreateTfilebytopics < ActiveRecord::Migration
  def change
    create_table :tfilebytopics do |t|
      t.integer   :ttopic_id
      t.string    :file_name
      t.string    :content_type
      t.binary    :data
      t.string    :created_by
      t.string    :updated_by
      
      t.timestamps
    end
  end
end
