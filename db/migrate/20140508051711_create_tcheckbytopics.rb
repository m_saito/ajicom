class CreateTcheckbytopics < ActiveRecord::Migration
  def change
    create_table :tcheckbytopics do |t|
      t.string :id
      t.integer :mcheck_id
      t.integer :ttopic_id
      t.string :name
      t.string :created_by
      t.string :updated_by

      t.timestamps
    end
  end
end
