class AddImageUrlToTfilebytopics < ActiveRecord::Migration
  def change
    add_column :tfilebytopics, :image_url, :string
  end
end
