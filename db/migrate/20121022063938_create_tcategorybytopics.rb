class CreateTcategorybytopics < ActiveRecord::Migration
  def change
    create_table :tcategorybytopics do |t|
      t.integer       :mcategory_id
      t.integer       :ttopic_id
      t.string        :created_by
      t.string        :updated_by
      
      t.timestamps
    end
  end
end
