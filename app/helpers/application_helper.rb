module ApplicationHelper
  def resource_name
    :user
  end

  def resource
     @resource ||= User.new
  end

  def devise_mapping
     @devise_mapping ||= Devise.mappings[:user]
  end
  
  IMAGE_TYPES = {'image/jpeg'=>true,'image/png'=>true,'image/jpg'=>true,'application/pdf'=>true}
  VIDEO_TYPES = {'video/mp4'=>true,'video/quicktime'=>true,'video/x-ms-wmv'=>true,'video/x-msvideo'=>true,'video/mpeg'=>true}
  
  def isAvailable(content_type)    
    return (IMAGE_TYPES[content_type] || VIDEO_TYPES[content_type])
  end
  
  def isImage(content_type)
    ret = false
    if IMAGE_TYPES[content_type]
      ret = true
    end
    return ret
  end
  
  def isPdf(content_type)
    ret = false
    if content_type == 'application/pdf'
      ret = true
    end
    return ret
  end
  
  def isMovie(content_type)
    ret = false
    if VIDEO_TYPES[content_type]
      ret = true
    end
    return ret
  end
end
