class Muser < ActiveRecord::Base
  attr_accessible :user_name, :user_kana, :mail, :user_div, :password
  
  has_many:tuserbycomps
  
  has_one:mcompany

  establish_connection "#{Rails.env}_other_database"
end
