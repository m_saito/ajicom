class Mcompany < ActiveRecord::Base
  attr_accessible :tworkareasubs,:company_name,:texpertises,:company_kana,:represent_name, :represent_kana, :division, :establish_date, :capital, :ajicom_users
  attr_accessible :zip_code, :address, :station, :employees, :web_url, :tel_no_1, :fax_no_1, :tel_no_2, :fax_no_2, :remarks, :account_opened, :contracted, :muser_id
  attr_accessible :tstaffs
  attr_accessible :historyback
  attr_accessible :company_type
  
  has_many:ttopics
  has_many:texpertbycomps
  has_many:tworkbycomps
  has_many:tuserbycomps
  has_many:tstaffbycomps
  
  belongs_to:muser
  
  serialize :historyback, String
  
  def historyback=(value)
    write_attribute(:historyback, value)
  end

  def historyback
    read_attribute(:historyback) || ""
  end
  
  serialize :tworkareasubs, Array
  
  def tworkareasubs=(value)
    write_attribute(:tworkareasubs, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def tworkareasubs
    read_attribute(:tworkareasubs) || []
  end
  
  serialize :tworkareas, Array
  
  def tworkareas=(value)
    write_attribute(:tworkareas, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def tworkareas
    read_attribute(:tworkareas) || []
  end  
  
  serialize :texpertises, Array
  
  def texpertises=(value)
    write_attribute(:texpertises, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def texpertises
    read_attribute(:texpertises) || []
  end  
  
  serialize :ajicom_users, Array
  
  def ajicom_users=(value)
    write_attribute(:ajicom_users, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def ajicom_users
    read_attribute(:ajicom_users) || []
  end
  
  serialize :tstaffs, Array
  
  def tstaffs=(value)
    write_attribute(:tstaffs, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def tstaffs
    read_attribute(:tstaffs) || []
  end
  
  
end
