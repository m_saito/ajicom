class Tcheckbytopic < ActiveRecord::Base
  #attr_accessible :created_by, :id, :mcheck_id, :name, :ttopic_id, :updated_by
  belongs_to:ttopic
  belongs_to:mcheck
  
  before_save do |record|
    if record.id.blank?
      record[:created_by] = User.current_user.email
    end
    record[:updated_by] = User.current_user.email
  end

end
