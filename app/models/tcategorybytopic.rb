class Tcategorybytopic < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to:ttopic
  belongs_to:mcategory
  
  before_save do |record|
    if record.id.blank?
      record[:created_by] = User.current_user.email
    end
    record[:updated_by] = User.current_user.email
  end

end
