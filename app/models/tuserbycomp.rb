class Tuserbycomp < ActiveRecord::Base
  # attr_accessible :title, :body
  
  before_save do |record|
    if record.id.blank?
      record[:created_by] = User.current_user.email
    end
    record[:updated_by] = User.current_user.email
  end
  
  belongs_to:mcompany
  belongs_to:muser
end
