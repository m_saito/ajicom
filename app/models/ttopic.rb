class Ttopic < ActiveRecord::Base
  attr_accessible :client, :brand, :tool, :body_text, :upload_date, :web_url, :tcategories, :category, :topic_type, :title, :upload_file,:client_id, :client_rep, :deployment_season, :mcompany_id, :printing_company, :production_cost, :tchecks, :ajicomsales, :cr_rep, :photo, :styling, :dep_season
  mount_uploader :upload_file, ImageUploader
  attr_accessible :historyback
  attr_accessible :use
  
  belongs_to:mcompany
  has_many:tfilebytopics
  has_one:mtopic_type
  has_many:tcategorybytopics
  has_many:tcheckbytopics

  before_save do |record|
    if record.id.blank?
      record[:created_by] = User.current_user.email
    end
    record[:updated_by] = User.current_user.email
  end
  
  serialize :historyback, String
  
  def historyback=(value)
    write_attribute(:historyback, value)
  end

  def historyback
    read_attribute(:historyback) || ""
  end
  
  serialize :tcategories, Array
  
  def tcategories=(value)
    write_attribute(:tcategories, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def tcategories
    read_attribute(:tcategories) || []
  end  

  serialize :tchecks, Array
  
  def tchecks=(value)
    write_attribute(:tchecks, Array.wrap(value).reject{|v| v.blank? }.map{|v| v.to_i })
  end

  def tchecks
    read_attribute(:tchecks) || []
  end  
  
end
