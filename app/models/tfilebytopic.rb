class Tfilebytopic < ActiveRecord::Base
  attr_accessible :upload_file,:upload_file_url,:thumbnail_url,:thumbnail2_url,:upload_file_no_image,:ttopic_id
  attr_accessible :upload_file_no_image_process, :upload_file_no_image_tmp
  attr_accessible :upload_file_process, :upload_file_tmp
  
  mount_uploader :upload_file, ImageUploader
  process_in_background :upload_file
  store_in_background :upload_file
  
  mount_uploader :upload_file_no_image, FileUploader
  process_in_background :upload_file_no_image
  store_in_background :upload_file_no_image
  
  belongs_to:ttopic
  
  before_save do |record|
    if record.id.blank?
      record[:created_by] = User.current_user.email
      record[:updated_by] = User.current_user.email
    end
  end

  include Rails.application.routes.url_helpers

  def to_jq_upload
    {
      
      "name" => read_attribute(:upload_file_name),
      "size" => read_attribute(:upload_file_size),
      "url" => read_attribute(:upload_file_url),
      "delete_url" => tfilebytopic_path(self),
      "delete_type" => "DELETE" 
    }
  end
  
end
