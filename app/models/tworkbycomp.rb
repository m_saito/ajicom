class Tworkbycomp < ActiveRecord::Base
  # attr_accessible :title, :body
  
  before_save do |record|
    if record.id.blank?
      record[:created_by] = User.current_user.email
    end
    record[:updated_by] = User.current_user.email
  end
  
  has_one:mcompany
  belongs_to:mworkarea
  belongs_to:mworkareasub
end
