class Mbrand < ActiveRecord::Base
  attr_accessible :brand_name
  validates :brand_name, uniqueness: true
end
