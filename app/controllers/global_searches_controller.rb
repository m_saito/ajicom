# -*- coding: utf-8 -*-
class GlobalSearchesController < ApplicationController
  
  TOPIC_TYPES = [[0,'アジコム作品の掲載'], [1,'その他の情報の掲載']]
  TOPIC_CATEGORIES = [[0,'家庭用'], [1,'業務用'], [2,'その他']]
  
  def search
    text = ''
    text = params[:global_search][:text] if params[:global_search]
    
    @mcompanies=[]
    @ttopics=[]
    
    if !text.blank?
      #協力会社
      @mcompanies = Mcompany.order('updated_at desc')
      t = Mcompany.arel_table
      
      #協力会社名、ふりがな、代表者氏名、代表者かな、住所、備考
      @mcompanies = @mcompanies.where(t[:company_name].matches("%#{text}%").or(t[:company_kana].matches("%#{text}%")).or(t[:represent_name].matches("%#{text}%")).or(t[:represent_kana].matches("%#{text}%")).or(t[:address].matches("%#{text}%")).or(t[:remarks].matches("%#{text}%")))

      #トピック
      #クライアント、ブランド、件名、ツール、内容
      @ttopics = Ttopic.order('updated_at desc')
      t = Ttopic.arel_table
      
      @ttopics= @ttopics.where(t[:client].matches("%#{text}%").or(t[:brand].matches("%#{text}%")).or(t[:title].matches("%#{text}%")).or(t[:tool].matches("%#{text}%")).or(t[:body_text].matches("%#{text}%")))

      #タイプ
      @topic_types = TOPIC_TYPES
  
      #カテゴリ
      @topic_categories = TOPIC_CATEGORIES

    end
    
    respond_to do |format|
      format.html
      format.json { render json: @mcompanies }
    end
  end
end
