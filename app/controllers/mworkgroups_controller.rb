class MworkgroupsController < ApplicationController
  # GET /mworkgroups
  # GET /mworkgroups.json
  def index
    @mworkgroups = Mworkgroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mworkgroups }
    end
  end

  # GET /mworkgroups/1
  # GET /mworkgroups/1.json
  def show
    @mworkgroup = Mworkgroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mworkgroup }
    end
  end

  # GET /mworkgroups/new
  # GET /mworkgroups/new.json
  def new
    @mworkgroup = Mworkgroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mworkgroup }
    end
  end

  # GET /mworkgroups/1/edit
  def edit
    @mworkgroup = Mworkgroup.find(params[:id])
  end

  # POST /mworkgroups
  # POST /mworkgroups.json
  def create
    @mworkgroup = Mworkgroup.new(params[:mworkgroup])

    respond_to do |format|
      if @mworkgroup.save
        format.html { redirect_to @mworkgroup, :notice => 'Mworkgroup was successfully created.' }
        format.json { render :json => @mworkgroup, :status => :created, :location => @mworkgroup }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mworkgroup.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mworkgroups/1
  # PUT /mworkgroups/1.json
  def update
    @mworkgroup = Mworkgroup.find(params[:id])

    respond_to do |format|
      if @mworkgroup.update_attributes(params[:mworkgroup])
        format.html { redirect_to @mworkgroup, :notice => 'Mworkgroup was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mworkgroup.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mworkgroups/1
  # DELETE /mworkgroups/1.json
  def destroy
    @mworkgroup = Mworkgroup.find(params[:id])
    @mworkgroup.destroy

    respond_to do |format|
      format.html { redirect_to mworkgroups_url }
      format.json { head :no_content }
    end
  end
end
