# -*- coding: utf-8 -*-
class WelcomeController < ApplicationController
  skip_filter :authenticate_user!
  def index    
    if user_signed_in?      
      #ユーザー情報がない場合は登録
      muser = Muser.where(:mail=>User.current_user.email).first
      if muser.blank?
        redirect_to new_muser_path
#      elsif muser.user_div == "0"
        #一般アジコムユーザー
        
        #協力会社情報がない場合は会社登録ページへ飛ばす
#        mcompany = Mcompany.where(:muser_id=>muser.id).first
#        if mcompany.blank?
#          redirect_to new_mcompany_path
#        else
#          redirect_to ttopics_path
#        end
      else
        #アジコムユーザー
        redirect_to whatsnew_ttopics_path
      end
    else
      logger.debug User.serialize_from_cookie('remember_user_token',nil)
    end
  end
end
