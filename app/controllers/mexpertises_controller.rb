class MexpertisesController < ApplicationController
  # GET /mexpertises
  # GET /mexpertises.json
  def index
    @mexpertises = Mexpertise.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mexpertises }
    end
  end

  # GET /mexpertises/1
  # GET /mexpertises/1.json
  def show
    @mexpertise = Mexpertise.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mexpertise }
    end
  end

  # GET /mexpertises/new
  # GET /mexpertises/new.json
  def new
    @mexpertise = Mexpertise.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mexpertise }
    end
  end

  # GET /mexpertises/1/edit
  def edit
    @mexpertise = Mexpertise.find(params[:id])
  end

  # POST /mexpertises
  # POST /mexpertises.json
  def create
    @mexpertise = Mexpertise.new(params[:mexpertise])

    respond_to do |format|
      if @mexpertise.save
        format.html { redirect_to @mexpertise, :notice => 'Mexpertise was successfully created.' }
        format.json { render :json => @mexpertise, :status => :created, :location => @mexpertise }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mexpertise.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mexpertises/1
  # PUT /mexpertises/1.json
  def update
    @mexpertise = Mexpertise.find(params[:id])

    respond_to do |format|
      if @mexpertise.update_attributes(params[:mexpertise])
        format.html { redirect_to @mexpertise, :notice => 'Mexpertise was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mexpertise.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mexpertises/1
  # DELETE /mexpertises/1.json
  def destroy
    @mexpertise = Mexpertise.find(params[:id])
    @mexpertise.destroy

    respond_to do |format|
      format.html { redirect_to mexpertises_url }
      format.json { head :no_content }
    end
  end
end
