class TcheckbytopicsController < ApplicationController
  # GET /tcheckbytopics
  # GET /tcheckbytopics.json
  def index
    @tcheckbytopics = Tcheckbytopic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tcheckbytopics }
    end
  end

  # GET /tcheckbytopics/1
  # GET /tcheckbytopics/1.json
  def show
    @tcheckbytopic = Tcheckbytopic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tcheckbytopic }
    end
  end

  # GET /tcheckbytopics/new
  # GET /tcheckbytopics/new.json
  def new
    @tcheckbytopic = Tcheckbytopic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tcheckbytopic }
    end
  end

  # GET /tcheckbytopics/1/edit
  def edit
    @tcheckbytopic = Tcheckbytopic.find(params[:id])
  end

  # POST /tcheckbytopics
  # POST /tcheckbytopics.json
  def create
    @tcheckbytopic = Tcheckbytopic.new(params[:tcheckbytopic])

    respond_to do |format|
      if @tcheckbytopic.save
        format.html { redirect_to @tcheckbytopic, notice: 'Tcheckbytopic was successfully created.' }
        format.json { render json: @tcheckbytopic, status: :created, location: @tcheckbytopic }
      else
        format.html { render action: "new" }
        format.json { render json: @tcheckbytopic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tcheckbytopics/1
  # PUT /tcheckbytopics/1.json
  def update
    @tcheckbytopic = Tcheckbytopic.find(params[:id])

    respond_to do |format|
      if @tcheckbytopic.update_attributes(params[:tcheckbytopic])
        format.html { redirect_to @tcheckbytopic, notice: 'Tcheckbytopic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tcheckbytopic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tcheckbytopics/1
  # DELETE /tcheckbytopics/1.json
  def destroy
    @tcheckbytopic = Tcheckbytopic.find(params[:id])
    @tcheckbytopic.destroy

    respond_to do |format|
      format.html { redirect_to tcheckbytopics_url }
      format.json { head :no_content }
    end
  end
end
