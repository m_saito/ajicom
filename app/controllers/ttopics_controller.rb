# -*- coding: utf-8 -*-
class TtopicsController < ApplicationController
  
  include ApplicationHelper
  
  #マスタテキスト用配列
  TOPIC_TYPES = [[0,'アジコム作品の掲載'], [1,'その他の情報の掲載']]
  TOPIC_CATEGORIES = [[0,'家庭用'], [1,'業務用'], [2,'その他']]
  
  # GET /ttopics
  # GET /ttopics.json
  def whatsnew
    muser = Muser.where(:mail=>User.current_user.email).first
    #get slideshow data
    @carousels = Tfilebytopic.where("content_type in ('image/jpg','image/jpeg','image/png')")
    @carousels = @carousels.where("ttopic_id is not null")
    # アジコム営業の場合、展開時期に達する作品のみ表示する
    if muser.user_div == '1'
      @carousels = @carousels.where("ttopic_id in (select id from ttopics where CURRENT_DATE >= dep_season)")
    end
    @carousels = @carousels.group(:ttopic_id).select('ttopic_id,min(id) as id,max(updated_at) as updated_at')
    @carousels = @carousels.order('updated_at desc').limit(5)

    #画像URLの組み立て
    @carousels.each do |carousel|
      tfile = Tfilebytopic.find(carousel.id) 
      carousel[:ttopic_id]=tfile.ttopic_id
      carousel[:img_url] = tfile.upload_file_url
#      carousel[:thumbnail_url] = tfile.thumbnail_url
      carousel[:thumbnail_url] = tfile.thumbnail2_url
      
      carousel[:title] = tfile.ttopic.title if tfile.ttopic
      carousel[:body_text] = tfile.ttopic.body_text if tfile.ttopic
      carousel[:mcompany] = tfile.ttopic.mcompany if tfile.ttopic
    end
    # アジコム営業の場合、展開時期に達する作品のみ表示する
    if muser.user_div == '1'
      @ttopics = Ttopic.where('CURRENT_DATE >= dep_season').order('updated_at desc').limit(12)
    # アジコム営業以外は、すべての作品を表示する
    else
      @ttopics = Ttopic.order('updated_at desc').limit(12)
    end
    
    #タイプ
    @topic_types = TOPIC_TYPES

    #カテゴリ
    @topic_categories = TOPIC_CATEGORIES
 
     respond_to do |format|
       format.html # index.html.erb
       format.json { render :json => @ttopics }
     end
  end
  
  # GET /mcompanies
  # GET /mcompanies.json
  def search
    muser = Muser.where(:mail=>User.current_user.email).first
    # アジコム営業の場合、展開時期に達する作品のみ表示する
    if muser.user_div == '1'
      @ttopics = Ttopic.where('CURRENT_DATE >= dep_season').order('updated_at desc').limit(12)
    # アジコム営業以外は、すべての作品を表示する
    else
      @ttopics = Ttopic.order('updated_at desc').limit(12)
    end
    
    @ttopic = Ttopic.new
    
    t = Ttopic.arel_table
    
    @tcategory_hash = {}
    @tcheck_hash = {}
    
    @params = params
    
    if !@params[:ttopic].blank?
      #検索
      if @params[:ttopic][:historyback] == "1"
        @params = session[:q]
      else
        session[:q] = @params
      end
      
      #タイプ
      #topic_type = @params[:ttopic][:topic_type]
      #if !topic_type.blank?
      #  @ttopics = @ttopics.where(:topic_type=>topic_type)
      #  @ttopic[:topic_type] = topic_type
      #end
      #日付
      #upload_date = @params[:ttopic][:upload_date]
      #if !upload_date.blank?
      #  @ttopic[:upload_date] = upload_date
      #  
      #  #Dateに戻す
      #  upload_date=Time.parse upload_date
      #  @ttopics = @ttopics.where(:upload_date=>upload_date)
      #end
      
      #クライアント
      #client = @params[:ttopic][:client]
      #if !client.blank?
      #  @ttopic[:client] = client
      #  
      #  @ttopics = @ttopics.where(t[:client].matches("%#{client}%"))
      #end

      #クライアント
      client_id = @params[:ttopic][:client_id]
      if !client_id.blank?
        @ttopic[:client_id] = client_id
        
        @ttopics = @ttopics.where(["client_id in (?)", client_id])
      end
      
      #製作会社
      mcompany_id = @params[:ttopic][:mcompany_id]
      if !mcompany_id.blank?
        @ttopic[:mcompany_id] = mcompany_id
        
        @ttopics = @ttopics.where(["mcompany_id in (?)", mcompany_id])
      end
      
      #印刷会社
      printing_company = @params[:ttopic][:printing_company]
      if !printing_company.blank?
        @ttopic[:printing_company] = printing_company
        
        @ttopics = @ttopics.where(["printing_company in (?)", printing_company])
      end
      
      #アジコム営業
      ajicomsales = @params[:ttopic][:ajicomsales]
      if !ajicomsales.blank?
        @ttopic[:ajicomsales] = ajicomsales
        
        @ttopics = @ttopics.where(["ajicomsales in (?)", ajicomsales])
      end
      
      #CR担当者
      cr_rep = @params[:ttopic][:cr_rep]
      if !cr_rep.blank?
        @ttopic[:cr_rep] = cr_rep
        
        @ttopics = @ttopics.where(["cr_rep in (?)", cr_rep])
      end
      
      #展開時期
      dep_season = @params[:ttopic][:dep_season]
      if !dep_season.blank?
        @ttopic[:dep_season] = dep_season
        
        @ttopics = @ttopics.where(t[:dep_season].matches("%#{dep_season}%"))
      end
      
      #カテゴリー
      #category = @params[:ttopic][:category]
      #if !category.blank?
      #  @ttopic[:category] = category
      #  @ttopics = @ttopics.where(t[:category].matches("%#{category}%"))
      #end

      #用途
      use = @params[:ttopic][:use]
      if !use.blank?
        @ttopic[:use] = use
        @ttopics = @ttopics.where(t[:use].matches("%#{use}%"))
      end
      
      #ブランド
      brand = @params[:ttopic][:brand]
      if !brand.blank?
        @ttopic[:brand] = brand
        @ttopics = @ttopics.where(t[:brand].matches("%#{brand}%"))
      end
      #件名
      title = @params[:ttopic][:title]
      if !title.blank?
        @ttopic[:title] = title
        @ttopics = @ttopics.where(t[:title].matches("%#{title}%"))
      end
      #ツール名
      tool = @params[:ttopic][:tool]
      if !tool.blank?
        @ttopic[:tool] = tool
        @ttopics = @ttopics.where(t[:tool].matches("%#{tool}%"))
      end
      #撮影
      photo = @params[:ttopic][:photo]
      if !photo.blank?
        @ttopic[:photo] = photo
        @ttopics = @ttopics.where(t[:photo].matches("%#{photo}%"))
      end
      #スタイリング
      styling = @params[:ttopic][:styling]
      if !styling.blank?
        @ttopic[:styling] = tool
        @ttopics = @ttopics.where(t[:styling].matches("%#{styling}%"))
      end
      
      #内容
      body_text = @params[:ttopic][:body_text]
      if !body_text.blank?
        @ttopic[:body_text] = body_text
        @ttopics = @ttopics.where(t[:body_text].matches("%#{body_text}%"))
      end

      #ツールカテゴリで絞り込み
      ttopic_ids = []
      if !@params[:tcategories].blank?
        tcategories = []
        @params[:tcategories].each do |k,v|
          if v == '1'
            tcategories.push k
            @tcategory_hash[k.to_i]=v
          end
        end
        if tcategories.count > 0
          tcategories = Tcategorybytopic.where(["mcategory_id in (?)", tcategories])
          tcategories.each do |tcategory|
            ttopic_ids.push tcategory.ttopic_id
          end
          
          if ttopic_ids.count > 0
            @ttopics = @ttopics.where(["id in (?)", ttopic_ids])
          else
            @ttopics = []
          end
        end
      end
      
      #特記事項で絞り込み
      ttopic_ids = []
      if !@params[:tchecks].blank?
        tchecks = []
        @params[:tchecks].each do |k,v|
          if v == '1'
            tchecks.push k
            @tcheck_hash[k.to_i]=v
          end
        end
        if tchecks.count > 0
          tchecks = Tcheckbytopic.where(["mcheck_id in (?)", tchecks])
          tchecks.each do |tcheck|
            ttopic_ids.push tcheck.ttopic_id
          end
          
          if ttopic_ids.count > 0
            @ttopics = @ttopics.where(["id in (?)", ttopic_ids])
          else
            @ttopics = []
          end
        end
      end
      
      
    else
      #初期表示
      @ttopics = @ttopics.limit(12)
    end

    @ttopics.each do |ttopic|
      #改行置き換え
      ttopic.body_text = ttopic.body_text.gsub '\r',"\n" if !ttopic.body_text.blank?
    end
    
    #カテゴリマスタ
    @mcategories = Mcategory.order(:id)
    
    #クラスセット
    @mcategories.each do |mcategory|
      mcategory[:class_string]='btn-topic'
      if @tcategory_hash[mcategory.id] == '1'
        mcategory[:class_string] += ' active '
        mcategory[:btn_active] = "1"
      else
        mcategory[:btn_active] = "0"
      end
    end

    #特記事項マスタ
    @mchecks = Mcheck.order(:id)
    
    #クラスセット
    @mchecks.each do |mcheck|
      mcheck[:class_string]='btn-topic'
      if @tcheck_hash[mcheck.id] == '1'
        mcheck[:class_string] += ' active '
        mcheck[:btn_active] = "1"
      else
        mcheck[:btn_active] = "0"
      end
    end    
    
    #タイプ
    @topic_types = TOPIC_TYPES

    #カテゴリ
    @topic_categories = TOPIC_CATEGORIES

    if !@params[:ttopic].blank?
      render
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render :json => @ttopics }
      end
    end
  end
  
  # GET /ttopics
  # GET /ttopics.json
  def history
    #履歴表示
    #muser = Muser.where(:mail=>User.current_user.email).first
    #if muser.user_div == '0'
      #協力会社ユーザー
    #  @ttopics = Ttopic.where(:created_by => User.current_user.email).paginate(:page => params[:page], :order => 'updated_at desc', :per_page => 10)
    #else
      #アジコムユーザー、システム管理者
      #mcompany_id = params[:mcompany_id]
      #if mcompany_id.blank?
        #デバッグ用
        #@ttopics = Ttopic.where(:created_by => User.current_user.email).paginate(:page => params[:page], :order => 'updated_at desc', :per_page => 10)
      #else
        #@ttopics = Ttopic.where(:mcompany_id => mcompany_id).paginate(:page => params[:page], :order => 'updated_at desc', :per_page => 10)
      #end
    #end

    muser = Muser.where(:mail=>User.current_user.email).first
    # アジコム営業の場合、展開時期に達する作品のみ表示する
    if muser.user_div == '1'
      @ttopics = Ttopic.where('CURRENT_DATE >= dep_season').paginate(:page => params[:page], :order => 'updated_at desc', :per_page => 15)
    # アジコム営業以外は、すべての作品を表示する
    else
      @ttopics = Ttopic.where("1=1").paginate(:page => params[:page], :order => 'updated_at desc', :per_page => 15)
    end
    
    @ttopics.each do |ttopic|
      #タイプ
      if ![ttopic.topic_type.to_i].blank?
        ttopic.topic_type = TOPIC_TYPES[ttopic.topic_type.to_i].last
      end
    end

    respond_to do |format|
       format.html # index.html.erb
       format.json { render :json => @ttopics }
     end
  end

  # GET /ttopics/1
  # GET /ttopics/1.json
  def show
    @ttopic = Ttopic.find(params[:id])
    
    #タイプ
    if ![@ttopic.topic_type.to_i].blank?
      @topic_type = TOPIC_TYPES[@ttopic.topic_type.to_i].last
    end
    
    #カテゴリ
    if ![@ttopic.category.to_i].blank?
      @topic_category = TOPIC_CATEGORIES[@ttopic.category.to_i].last
    end
    
    #改行コード変換
    @ttopic.body_text = @ttopic.body_text.gsub '\r',"\n" if !@ttopic.body_text.blank?
    
    #idの配列に変換
    @ttopic.tcategories = []
    @ttopic.tcategorybytopics.each do |tcategory|
      @ttopic.tcategories.push tcategory.mcategory_id
    end

    #idの配列に変換
    @ttopic.tchecks = []
    @ttopic.tcheckbytopics.each do |tcheck|
      @ttopic.tchecks.push tcheck.mcheck_id
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @ttopic }
    end
  end

  # GET /ttopics/new
  # GET /ttopics/new.json
  def new
    @ttopic = Ttopic.new
    
    @ttopic.topic_type = 0
    @ttopic.category = 0
    
    @ttopic.upload_date=Time.now.strftime('%Y/%m/%d')
    
    #タイプ
    @topic_types = TOPIC_TYPES

    #カテゴリ
    @topic_categories = TOPIC_CATEGORIES
    
    @client_rep = Tstaffbycomp.find(:all)
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @ttopic }
    end
  end

  # GET /ttopics/1/edit
  def edit
    
    @ttopic = Ttopic.find(params[:id])
    #コンボ用データ取得
    @brand = Mbrand.order(:id)
    @client_id = Mcompany.where(:company_type => 1).order(:id)
    @mcompany_id = Mcompany.where("company_type = 0").order(:id)
    @printing_company = Mcompany.where(:company_type => 2).order(:id)
    #タイプ
    @topic_types = TOPIC_TYPES

    #カテゴリ
    @topic_categories = TOPIC_CATEGORIES
    
    if !@ttopic.upload_date.blank?
      @ttopic.upload_date=@ttopic.upload_date.strftime('%Y/%m/%d')
    end
    
    #改行コード変換
    @ttopic.body_text = @ttopic.body_text.gsub '\r',"\n" if !@ttopic.body_text.blank?
    
    #idの配列に変換
    @ttopic.tcategories = []
    @ttopic.tcategorybytopics.each do |tcategory|
      @ttopic.tcategories.push tcategory.mcategory_id
    end

    #idの配列に変換
    @ttopic.tchecks = []
    @ttopic.tcheckbytopics.each do |tcheck|
      @ttopic.tchecks.push tcheck.mcheck_id
    end
    
    @tfilebytopics = []
    @ttopic.tfilebytopics.each do |tfile|
      @tfilebytopics.push tfile.to_jq_upload.to_json
    end
    
    respond_to do |format|
      format.html
      format.json { render json:@ttopic.tfilebytopics.map{|upload| upload.to_jq_upload }}
    end
    
  end
  
  # GET /ttopics
  # POST /ttopics.json
  def index
    #新規登録画面用(FileUploadの制限のため)
    session[:tfilebytopics] = []
    
    @ttopic = Ttopic.new

    #コンボ用データ取得
    @brand = Mbrand.order(:id)
    @client_id = Mcompany.where(:company_type => 1).order(:id)
    @mcompany_id = Mcompany.where("company_type = 0").order(:id)
    @printing_company = Mcompany.where(:company_type => 2).order(:id)
    
    @ttopic.topic_type = 0
    @ttopic.category = 0
    
    @ttopic.upload_date=Time.now.strftime('%Y/%m/%d')
 
    #タイプ
    @topic_types = TOPIC_TYPES

    #カテゴリ
    @topic_categories = TOPIC_CATEGORIES
    
    @client_rep = []
    
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # POST /ttopics
  # POST /ttopics.json
  def create
    
    @upload_file = params[:ttopic][:upload_file]
    
    #ファイル
    session[:tfilebytopics] ||= []
    tfilebytopics = session[:tfilebytopics]
 
    if @upload_file
      #ファイル登録
      @tfilebytopic = Tfilebytopic.new
      
      @tfilebytopic[:ttopic_id] = @ttopic_id
     
      upload_file = @upload_file[0]
      
      filename=''
      content_type=@upload_file[0].content_type
      size=0

      #トランザクション開始
      begin
        ActiveRecord::Base::transaction() do
          
          if isImage(content_type)
            @tfilebytopic.process_upload_file_upload = true
            @tfilebytopic.upload_file = @upload_file[0]
            filename = @tfilebytopic.upload_file.file.filename
            content_type = @tfilebytopic.upload_file.file.content_type
            size =  @tfilebytopic.upload_file.file.size
          else
            @tfilebytopic.process_upload_file_no_image_upload = true
            #画像以外の場合
            @tfilebytopic.upload_file_no_image = @upload_file[0]
            filename = @tfilebytopic.upload_file_no_image.file.filename
            content_type = @tfilebytopic.upload_file_no_image.file.content_type
            size =  @tfilebytopic.upload_file_no_image.file.size
          end
          
          @tfilebytopic.upload_file_name = filename  

          respond_to do |format|  
              
            format.json { render json: [@tfilebytopic.to_jq_upload].to_json, status: :accepted, location: @ttopic }
            
            #env["rack_after_reply.callbacks"] << lambda {
              if @tfilebytopic.save
                #ファイル情報を保存しておく
                if isImage(content_type)
                  @tfilebytopic.upload_file_url = @tfilebytopic.upload_file.url
                  
                  @tfilebytopic.thumbnail_url = @tfilebytopic.upload_file.thumb.url
                  @tfilebytopic.thumbnail2_url = @tfilebytopic.upload_file.thumb2.url
                else
                  @tfilebytopic.thumbnail_url = @tfilebytopic.upload_file_no_image.url
                  @tfilebytopic.thumbnail2_url = @tfilebytopic.upload_file_no_image.url
                end
   
                @tfilebytopic.content_type = content_type
                @tfilebytopic.upload_file_size = size
   
                @tfilebytopic.save
                
                tfilebytopics.push @tfilebytopic.id
                session[:tfilebytopics] = tfilebytopics
              end
            #}
            
          end
          
        end #commit

      rescue => exc
        # 例外が発生したときの処理
        logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
        logger.fatal exc
        logger.fatal exc.message
        logger.fatal exc.backtrace.join("\n")
        
        respond_to do |format|
          format.html
          format.json { render :json => @ttopic.errors, :status => :unprocessable_entity }
        end
      end
    else
      #トピック登録
      @ttopic = Ttopic.new(params[:ttopic])
      
      tcategories = params[:ttopic][:tcategories]
      tchecks = params[:ttopic][:tchecks]

      
      #協力会社IDをセット
      muser = Muser.where(:mail=>User.current_user.email).first
      @ttopic.mcompany_id = muser.mcompany.id if muser && muser.mcompany
      
      #データを有効化
      @ttopic.enabled = 1
     
      #トランザクション開始
      begin
        ActiveRecord::Base::transaction() do
  
          respond_to do |format|       
            if @ttopic.save
              #カテゴリをdelete,insertする
              Tcategorybytopic.destroy_all(:ttopic_id => @ttopic.id)
               
              tcategories.each do |category_id|
                if !category_id.blank?
                  tcategory = Tcategorybytopic.new
                  tcategory[:ttopic_id] = @ttopic.id
                  tcategory[:mcategory_id] = category_id.to_i
                  tcategory.save
                end
              end
              
              #特記事項をdelete,insertする
              Tcheckbytopic.destroy_all(:ttopic_id => @ttopic.id)

              tchecks.each do |check_id|
                if !check_id.blank?
                  tcheck = Tcheckbytopic.new
                  tcheck[:ttopic_id] = @ttopic.id
                  tcheck[:mcheck_id] = check_id
                  tcheck.save
                end
              end
              
              #ファイルを更新
              tfilebytopics.each do |tfile_id|
                tfile = Tfilebytopic.find(tfile_id)
                tfile.ttopic_id = @ttopic.id
                tfile.save
              end
              
              session[:tfilebytopics] = nil
              
              #メール送信
              #TtopicMailer.regist_mail(@ttopic).deliver
              
              format.html { redirect_to @ttopic, :notice => '作品、情報をアップロードしました。' }
              format.json { render json: [@ttopic.to_jq_upload].to_json, status: :created, location: @ttopic }
              
            else
              format.html { redirect_to @ttopic, :notice => '登録に失敗しました。' }
              format.json { render :json => @ttopic.errors, :status => :unprocessable_entity }
            end
          end
          
        end #commit

      rescue => exc
        # 例外が発生したときの処理
        logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
        logger.fatal exc
        logger.fatal exc.message
        logger.fatal exc.backtrace.join("\n")
        
        respond_to do |format|
          format.html { redirect_to @ttopic, :notice => '登録に失敗しました。' }
          format.json { render :json => @ttopic.errors, :status => :unprocessable_entity }
        end
      end
     end
  end

  # PUT /ttopics/1
  # PUT /ttopics/1.json
  def update
    @upload_file = params[:ttopic][:upload_file]
    
    @ttopic_id = params[:id]
 
    if @upload_file
      #ファイル登録
      @tfilebytopic = Tfilebytopic.new
      
      @tfilebytopic[:ttopic_id] = @ttopic_id
     
      upload_file = @upload_file[0]
      
      filename=''
      content_type=@upload_file[0].content_type
      size=0

      #トランザクション開始
      begin
        ActiveRecord::Base::transaction() do
          
          if isImage(content_type)
            @tfilebytopic.process_upload_file_upload = true
            @tfilebytopic.upload_file = @upload_file[0]
            filename = @tfilebytopic.upload_file.file.filename
            content_type = @tfilebytopic.upload_file.file.content_type
            size =  @tfilebytopic.upload_file.file.size
            logger.info size
          else
            @tfilebytopic.process_upload_file_no_image_upload = true
            #画像以外の場合
            @tfilebytopic.upload_file_no_image = @upload_file[0]
            filename = @tfilebytopic.upload_file_no_image.file.filename
            content_type = @tfilebytopic.upload_file_no_image.file.content_type
            size =  @tfilebytopic.upload_file_no_image.file.size
          end
          
          @tfilebytopic.upload_file_name = filename  

          respond_to do |format|  
              
            format.json { render json: [@tfilebytopic.to_jq_upload].to_json, status: :accepted, location: @ttopic }
            
            #env["rack_after_reply.callbacks"] << lambda {
              if @tfilebytopic.save
                #ファイル情報を保存しておく
                if isImage(content_type)
                  @tfilebytopic.upload_file_url = @tfilebytopic.upload_file.url
                  
                  @tfilebytopic.thumbnail_url = @tfilebytopic.upload_file.thumb.url
                  @tfilebytopic.thumbnail2_url = @tfilebytopic.upload_file.thumb2.url
                else
                  @tfilebytopic.thumbnail_url = @tfilebytopic.upload_file_no_image.url
                  @tfilebytopic.thumbnail2_url = @tfilebytopic.upload_file_no_image.url
                end
   
                @tfilebytopic.content_type = content_type
                @tfilebytopic.upload_file_size = size
   
                @tfilebytopic.save
              end
            #}
            
          end
          
        end #commit

      rescue => exc
        # 例外が発生したときの処理
        logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
        logger.fatal exc
        logger.fatal exc.message
        logger.fatal exc.backtrace.join("\n")
        
       respond_to do |format|
          format.html
          format.json { render :json => @tfilebytopic.errors, :status => :unprocessable_entity }
       end
      end
    else
      #トピック登録
      @ttopic = Ttopic.find(params[:id])
      logger.info params
      tcategories = params[:ttopic][:tcategories]
      tchecks = params[:ttopic][:tchecks]
      
      
      #データを有効化
      @ttopic.enabled = 1
     
      #トランザクション開始
      begin
        ActiveRecord::Base::transaction() do
  
          respond_to do |format|       
            if @ttopic.update_attributes(params[:ttopic])
              #カテゴリをdelete,insertする
              Tcategorybytopic.destroy_all(:ttopic_id => @ttopic.id)
               
              tcategories.each do |category_id|
                if !category_id.blank?
                  tcategory = Tcategorybytopic.new
                  tcategory[:ttopic_id] = @ttopic.id
                  tcategory[:mcategory_id] = category_id.to_i
                  tcategory.save
                end
              end
              
              #特記事項をdelete,insertする
              Tcheckbytopic.destroy_all(:ttopic_id => @ttopic.id)
              
              tchecks.each do |check_id|
                if !check_id.blank?
                  tcheck = Tcheckbytopic.new
                  tcheck[:ttopic_id] = @ttopic.id
                  tcheck[:mcheck_id] = check_id
                  tcheck.save
                end
              end
              
              #ファイルを更新
              @ttopic.tfilebytopics.each do |tfile|
                tfile.ttopic_id = @ttopic.id
                tfile.save
              end
              
              format.html { redirect_to @ttopic, :notice => '作品、情報をアップロードしました。' }
              format.json { render json: [@ttopic.to_jq_upload].to_json, status: :created, location: @ttopic }
              
            else
              format.html
              format.json { render :json => @ttopic.errors, :status => :unprocessable_entity }
            end
          end
          
          #TODO 新規登録時はメールを飛ばす
          
       end #commit

      rescue => exc
        # 例外が発生したときの処理
        logger.info '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
        logger.info exc
        logger.info exc.message
        logger.info exc.backtrace.join("\n")
        
        respond_to do |format|
          format.html
          format.json { render :json => @ttopic.errors, :status => :unprocessable_entity }
        end
      end
     end
  end

  # DELETE /ttopics/1
  # DELETE /ttopics/1.json
  def destroy
    @ttopic = Ttopic.find(params[:id])
    
    Tfilebytopic.delete_all(:ttopic_id=>@ttopic.id)
    
    @ttopic.destroy

    respond_to do |format|
      format.html { redirect_to ttopics_url }
      format.json { head :no_content }
    end
  end

# GET /jushos/todoufuken_select
  def client_select
    @client_rep = Tstaffbycomp.find_all_by_mcompany_id(params[:client_id])

    respond_to do |format|
      format.json  { render :json => @client_rep }
    end
  end

  # GET /ttopics/cmbbrand
  def cmbbrand
    @brand = Mbrand.order(:id)
    respond_to do |format|
      format.json  { render :json => @brand }
    end
  end
  
  # GET /ttopics/cmbclient_id
  def cmbclient_id
    @cmbvalue = Mcompany.where(:company_type => 1).order(:id)
    respond_to do |format|
      format.json  { render :json => @cmbvalue }
    end
  end
  
  # GET /ttopics/cmbmcompany_id
  def cmbmcompany_id
    @cmbvalue = Mcompany.where("company_type = 0").order(:id)
    respond_to do |format|
      format.json  { render :json => @cmbvalue }
    end
  end
  
  # GET /ttopics/cmbmprinting_company
  def cmbmprinting_company
    @cmbvalue = Mcompany.where(:company_type => 2).order(:id)
    respond_to do |format|
      format.json  { render :json => @cmbvalue }
    end
  end
  
end
