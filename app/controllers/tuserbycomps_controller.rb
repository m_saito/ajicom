class TuserbycompsController < ApplicationController
  # GET /tuserbycomps
  # GET /tuserbycomps.json
  def index
    @tuserbycomps = Tuserbycomp.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @tuserbycomps }
    end
  end

  # GET /tuserbycomps/1
  # GET /tuserbycomps/1.json
  def show
    @tuserbycomp = Tuserbycomp.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @tuserbycomp }
    end
  end

  # GET /tuserbycomps/new
  # GET /tuserbycomps/new.json
  def new
    @tuserbycomp = Tuserbycomp.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @tuserbycomp }
    end
  end

  # GET /tuserbycomps/1/edit
  def edit
    @tuserbycomp = Tuserbycomp.find(params[:id])
  end

  # POST /tuserbycomps
  # POST /tuserbycomps.json
  def create
    @tuserbycomp = Tuserbycomp.new(params[:tuserbycomp])

    respond_to do |format|
      if @tuserbycomp.save
        format.html { redirect_to @tuserbycomp, :notice => 'Tuserbycomp was successfully created.' }
        format.json { render :json => @tuserbycomp, :status => :created, :location => @tuserbycomp }
      else
        format.html { render :action => "new" }
        format.json { render :json => @tuserbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /tuserbycomps/1
  # PUT /tuserbycomps/1.json
  def update
    @tuserbycomp = Tuserbycomp.find(params[:id])

    respond_to do |format|
      if @tuserbycomp.update_attributes(params[:tuserbycomp])
        format.html { redirect_to @tuserbycomp, :notice => 'Tuserbycomp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @tuserbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /tuserbycomps/1
  # DELETE /tuserbycomps/1.json
  def destroy
    @tuserbycomp = Tuserbycomp.find(params[:id])
    @tuserbycomp.destroy

    respond_to do |format|
      format.html { redirect_to tuserbycomps_url }
      format.json { head :no_content }
    end
  end
end
