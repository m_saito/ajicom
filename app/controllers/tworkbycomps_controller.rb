class TworkbycompsController < ApplicationController
  # GET /tworkbycomps
  # GET /tworkbycomps.json
  def index
    @tworkbycomps = Tworkbycomp.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @tworkbycomps }
    end
  end

  # GET /tworkbycomps/1
  # GET /tworkbycomps/1.json
  def show
    @tworkbycomp = Tworkbycomp.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @tworkbycomp }
    end
  end

  # GET /tworkbycomps/new
  # GET /tworkbycomps/new.json
  def new
    @tworkbycomp = Tworkbycomp.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @tworkbycomp }
    end
  end

  # GET /tworkbycomps/1/edit
  def edit
    @tworkbycomp = Tworkbycomp.find(params[:id])
  end

  # POST /tworkbycomps
  # POST /tworkbycomps.json
  def create
    @tworkbycomp = Tworkbycomp.new(params[:tworkbycomp])

    respond_to do |format|
      if @tworkbycomp.save
        format.html { redirect_to @tworkbycomp, :notice => 'Tworkbycomp was successfully created.' }
        format.json { render :json => @tworkbycomp, :status => :created, :location => @tworkbycomp }
      else
        format.html { render :action => "new" }
        format.json { render :json => @tworkbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /tworkbycomps/1
  # PUT /tworkbycomps/1.json
  def update
    @tworkbycomp = Tworkbycomp.find(params[:id])

    respond_to do |format|
      if @tworkbycomp.update_attributes(params[:tworkbycomp])
        format.html { redirect_to @tworkbycomp, :notice => 'Tworkbycomp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @tworkbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /tworkbycomps/1
  # DELETE /tworkbycomps/1.json
  def destroy
    @tworkbycomp = Tworkbycomp.find(params[:id])
    @tworkbycomp.destroy

    respond_to do |format|
      format.html { redirect_to tworkbycomps_url }
      format.json { head :no_content }
    end
  end
end
