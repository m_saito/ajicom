# -*- coding: utf-8 -*-
class UsersController < ApplicationController
  skip_filter :authenticate_user!,:except=>:index
  def index 
    @users = User.all
  end
 
  def new
    @user = User.new
  end
  
  #ユーザー作成
  def create
    @user = User.new(params[:user])
#    @user[:expiration_at] = 180.days.ago
    respond_to do |format|
      if @user.save
        format.html { redirect_to '/users/sign_in', :notice=>'確認メールを送信しました。登録されたメールアドレスに記載されたリンクをクリックしてログインして下さい。' }
      else
        format.html { render '/users/new' }
      end
    end
  end
  
  #ユーザー削除
  def destroy
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.destroy
        format.html { redirect_to users_path }
      else
        format.html { render users_path }
      end
    end
  end
end
