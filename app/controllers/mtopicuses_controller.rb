class MtopicusesController < ApplicationController
  # GET /mtopicuses
  # GET /mtopicuses.json
  def index
    @mtopicuses = Mtopicuses.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mtopicuses }
    end
  end

  # GET /mtopicuses/1
  # GET /mtopicuses/1.json
  def show
    @mtopicuses = Mtopicuses.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mtopicuses }
    end
  end

  # GET /mtopicuses/new
  # GET /mtopicuses/new.json
  def new
    @mtopicuses = Mtopicuses.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mtopicuses }
    end
  end

  # GET /mtopicuses/1/edit
  def edit
    @mtopicuses = Mtopicuses.find(params[:id])
  end

  # POST /mtopicuses
  # POST /mtopicuses.json
  def create
    @mtopicuses = Mtopicuses.new(params[:mcompanytype])

    respond_to do |format|
      if @mtopicuses.save
        format.html { redirect_to @mtopicuses, notice: 'Mtopicuses was successfully created.' }
        format.json { render json: @mtopicuses, status: :created, location: @mtopicuses }
      else
        format.html { render action: "new" }
        format.json { render json: @mtopicuses.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mtopicuses/1
  # PUT /mtopicuses/1.json
  def update
    @mtopicuses = Mtopicuses.find(params[:id])

    respond_to do |format|
      if @mtopicuses.update_attributes(params[:mcompanytype])
        format.html { redirect_to @mtopicuses, notice: 'Mtopicuses was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mtopicuses.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mtopicuses/1
  # DELETE /mtopicuses/1.json
  def destroy
    @mtopicuses = Mtopicuses.find(params[:id])
    @mtopicuses.destroy

    respond_to do |format|
      format.html { redirect_to mcompanytypes_url }
      format.json { head :no_content }
    end
  end
end
