class VdivbycompsController < ApplicationController
  # GET /vdivbycomps
  # GET /vdivbycomps.json
  def index
    @vdivbycomps = Vdivbycomp.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @vdivbycomps }
    end
  end

  # GET /vdivbycomps/1
  # GET /vdivbycomps/1.json
  def show
    @vdivbycomp = Vdivbycomp.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @vdivbycomp }
    end
  end

  # GET /vdivbycomps/new
  # GET /vdivbycomps/new.json
  def new
    @vdivbycomp = Vdivbycomp.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @vdivbycomp }
    end
  end

  # GET /vdivbycomps/1/edit
  def edit
    @vdivbycomp = Vdivbycomp.find(params[:id])
  end

  # POST /vdivbycomps
  # POST /vdivbycomps.json
  def create
    @vdivbycomp = Vdivbycomp.new(params[:vdivbycomp])

    respond_to do |format|
      if @vdivbycomp.save
        format.html { redirect_to @vdivbycomp, :notice => 'Vdivbycomp was successfully created.' }
        format.json { render :json => @vdivbycomp, :status => :created, :location => @vdivbycomp }
      else
        format.html { render :action => "new" }
        format.json { render :json => @vdivbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /vdivbycomps/1
  # PUT /vdivbycomps/1.json
  def update
    @vdivbycomp = Vdivbycomp.find(params[:id])

    respond_to do |format|
      if @vdivbycomp.update_attributes(params[:vdivbycomp])
        format.html { redirect_to @vdivbycomp, :notice => 'Vdivbycomp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @vdivbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /vdivbycomps/1
  # DELETE /vdivbycomps/1.json
  def destroy
    @vdivbycomp = Vdivbycomp.find(params[:id])
    @vdivbycomp.destroy

    respond_to do |format|
      format.html { redirect_to vdivbycomps_url }
      format.json { head :no_content }
    end
  end
end
