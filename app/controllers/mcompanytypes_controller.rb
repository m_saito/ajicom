class McompanytypesController < ApplicationController
  # GET /mcompanytypes
  # GET /mcompanytypes.json
  def index
    @mcompanytypes = Mcompanytype.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mcompanytypes }
    end
  end

  # GET /mcompanytypes/1
  # GET /mcompanytypes/1.json
  def show
    @mcompanytype = Mcompanytype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mcompanytype }
    end
  end

  # GET /mcompanytypes/new
  # GET /mcompanytypes/new.json
  def new
    @mcompanytype = Mcompanytype.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mcompanytype }
    end
  end

  # GET /mcompanytypes/1/edit
  def edit
    @mcompanytype = Mcompanytype.find(params[:id])
  end

  # POST /mcompanytypes
  # POST /mcompanytypes.json
  def create
    @mcompanytype = Mcompanytype.new(params[:mcompanytype])

    respond_to do |format|
      if @mcompanytype.save
        format.html { redirect_to @mcompanytype, notice: 'Mcompanytype was successfully created.' }
        format.json { render json: @mcompanytype, status: :created, location: @mcompanytype }
      else
        format.html { render action: "new" }
        format.json { render json: @mcompanytype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mcompanytypes/1
  # PUT /mcompanytypes/1.json
  def update
    @mcompanytype = Mcompanytype.find(params[:id])

    respond_to do |format|
      if @mcompanytype.update_attributes(params[:mcompanytype])
        format.html { redirect_to @mcompanytype, notice: 'Mcompanytype was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mcompanytype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mcompanytypes/1
  # DELETE /mcompanytypes/1.json
  def destroy
    @mcompanytype = Mcompanytype.find(params[:id])
    @mcompanytype.destroy

    respond_to do |format|
      format.html { redirect_to mcompanytypes_url }
      format.json { head :no_content }
    end
  end
end
