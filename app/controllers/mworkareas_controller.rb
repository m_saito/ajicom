class MworkareasController < ApplicationController
  # GET /mworkareas
  # GET /mworkareas.json
  def index
    @mworkareas = Mworkarea.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mworkareas }
    end
  end

  # GET /mworkareas/1
  # GET /mworkareas/1.json
  def show
    @mworkarea = Mworkarea.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mworkarea }
    end
  end

  # GET /mworkareas/new
  # GET /mworkareas/new.json
  def new
    @mworkarea = Mworkarea.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mworkarea }
    end
  end

  # GET /mworkareas/1/edit
  def edit
    @mworkarea = Mworkarea.find(params[:id])
  end

  # POST /mworkareas
  # POST /mworkareas.json
  def create
    @mworkarea = Mworkarea.new(params[:mworkarea])

    respond_to do |format|
      if @mworkarea.save
        format.html { redirect_to @mworkarea, :notice => 'Mworkarea was successfully created.' }
        format.json { render :json => @mworkarea, :status => :created, :location => @mworkarea }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mworkarea.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mworkareas/1
  # PUT /mworkareas/1.json
  def update
    @mworkarea = Mworkarea.find(params[:id])

    respond_to do |format|
      if @mworkarea.update_attributes(params[:mworkarea])
        format.html { redirect_to @mworkarea, :notice => 'Mworkarea was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mworkarea.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mworkareas/1
  # DELETE /mworkareas/1.json
  def destroy
    @mworkarea = Mworkarea.find(params[:id])
    @mworkarea.destroy

    respond_to do |format|
      format.html { redirect_to mworkareas_url }
      format.json { head :no_content }
    end
  end
end
