class TcategorybytopicsController < ApplicationController
  # GET /tcategorybytopics
  # GET /tcategorybytopics.json
  def index
    @tcategorybytopics = Tcategorybytopic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @tcategorybytopics }
    end
  end

  # GET /tcategorybytopics/1
  # GET /tcategorybytopics/1.json
  def show
    @tcategorybytopic = Tcategorybytopic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @tcategorybytopic }
    end
  end

  # GET /tcategorybytopics/new
  # GET /tcategorybytopics/new.json
  def new
    @tcategorybytopic = Tcategorybytopic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @tcategorybytopic }
    end
  end

  # GET /tcategorybytopics/1/edit
  def edit
    @tcategorybytopic = Tcategorybytopic.find(params[:id])
  end

  # POST /tcategorybytopics
  # POST /tcategorybytopics.json
  def create
    @tcategorybytopic = Tcategorybytopic.new(params[:tcategorybytopic])

    respond_to do |format|
      if @tcategorybytopic.save
        format.html { redirect_to @tcategorybytopic, :notice => 'Tcategorybytopic was successfully created.' }
        format.json { render :json => @tcategorybytopic, :status => :created, :location => @tcategorybytopic }
      else
        format.html { render :action => "new" }
        format.json { render :json => @tcategorybytopic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /tcategorybytopics/1
  # PUT /tcategorybytopics/1.json
  def update
    @tcategorybytopic = Tcategorybytopic.find(params[:id])

    respond_to do |format|
      if @tcategorybytopic.update_attributes(params[:tcategorybytopic])
        format.html { redirect_to @tcategorybytopic, :notice => 'Tcategorybytopic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @tcategorybytopic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /tcategorybytopics/1
  # DELETE /tcategorybytopics/1.json
  def destroy
    @tcategorybytopic = Tcategorybytopic.find(params[:id])
    @tcategorybytopic.destroy

    respond_to do |format|
      format.html { redirect_to tcategorybytopics_url }
      format.json { head :no_content }
    end
  end
end
