class TstaffbycompsController < ApplicationController
  # GET /tstaffbycomps
  # GET /tstaffbycomps.json
  def index
    @tstaffbycomps = Tstaffbycomp.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tstaffbycomps }
    end
  end

  # GET /tstaffbycomps/1
  # GET /tstaffbycomps/1.json
  def show
    @tstaffbycomp = Tstaffbycomp.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tstaffbycomp }
    end
  end

  # GET /tstaffbycomps/new
  # GET /tstaffbycomps/new.json
  def new
    @tstaffbycomp = Tstaffbycomp.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tstaffbycomp }
    end
  end

  # GET /tstaffbycomps/1/edit
  def edit
    @tstaffbycomp = Tstaffbycomp.find(params[:id])
  end

  # POST /tstaffbycomps
  # POST /tstaffbycomps.json
  def create
    @tstaffbycomp = Tstaffbycomp.new(params[:tstaffbycomp])

    respond_to do |format|
      if @tstaffbycomp.save
        format.html { redirect_to @tstaffbycomp, notice: 'Tstaffbycomp was successfully created.' }
        format.json { render json: @tstaffbycomp, status: :created, location: @tstaffbycomp }
      else
        format.html { render action: "new" }
        format.json { render json: @tstaffbycomp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tstaffbycomps/1
  # PUT /tstaffbycomps/1.json
  def update
    @tstaffbycomp = Tstaffbycomp.find(params[:id])

    respond_to do |format|
      if @tstaffbycomp.update_attributes(params[:tstaffbycomp])
        format.html { redirect_to @tstaffbycomp, notice: 'Tstaffbycomp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tstaffbycomp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tstaffbycomps/1
  # DELETE /tstaffbycomps/1.json
  def destroy
    @tstaffbycomp = Tstaffbycomp.find(params[:id])
    @tstaffbycomp.destroy

    respond_to do |format|
      format.html { redirect_to tstaffbycomps_url }
      format.json { head :no_content }
    end
  end
end
