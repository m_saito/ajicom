# -*- coding: utf-8 -*-
class MbrandsController < ApplicationController

  # GET /mbrands
  # GET /mbrands.json
  def index
    @mbrands = Mbrand.order(:id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mbrands }
    end
  end

  # GET /mbrands/new
  # GET /mbrands/new.json
  def new
    @mbrand = Mbrand.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mbrand }
    end
  end

  # GET /mbrands/popup
  # GET /mbrands/popup.json
  def popup
    if !params[:mbrand].blank?
      @mbrand = Mbrand.new(params[:mbrand])
      @mbrand.save
      render
    else
      @mbrand = Mbrand.new
      render :layout => 'popup'
    end
  end

  # GET /mbrands/1/edit
  def edit
    @mbrand = Mbrand.find(params[:id])
  end

  # POST /mbrands
  # POST /mbrands.json
  def create
    @mbrand = Mbrand.new(params[:mbrand])

    respond_to do |format|
      if @mbrand.save
        format.html { redirect_to action: :index }
        format.json { head :no_content }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mbrand.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mbrands/1
  # PUT /mbrands/1.json
  def update
    @mbrand = Mbrand.find(params[:id])

    respond_to do |format|
      if @mbrand.update_attributes(params[:mbrand])
        format.html { redirect_to action: :index }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mbrand.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mbrands/1
  # DELETE /mbrands/1.json
  def destroy
    @mbrand = Mbrand.find(params[:id])

    begin
      ActiveRecord::Base::transaction() do
        #ブランド情報削除
        @mbrand.destroy
      end
    rescue => exc
      # 例外が発生したときの処理
      logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
      logger.fatal exc
      logger.fatal exc.message
      logger.fatal exc.backtrace.join("\n")
    end

    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { head :no_content }
    end
  end
end
