class MworkareasubsController < ApplicationController
  # GET /mworkareasubs
  # GET /mworkareasubs.json
  def index
    @mworkareasubs = Mworkareasub.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mworkareasubs }
    end
  end

  # GET /mworkareasubs/1
  # GET /mworkareasubs/1.json
  def show
    @mworkareasub = Mworkareasub.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mworkareasub }
    end
  end

  # GET /mworkareasubs/new
  # GET /mworkareasubs/new.json
  def new
    @mworkareasub = Mworkareasub.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mworkareasub }
    end
  end

  # GET /mworkareasubs/1/edit
  def edit
    @mworkareasub = Mworkareasub.find(params[:id])
  end

  # POST /mworkareasubs
  # POST /mworkareasubs.json
  def create
    @mworkareasub = Mworkareasub.new(params[:mworkareasub])

    respond_to do |format|
      if @mworkareasub.save
        format.html { redirect_to @mworkareasub, :notice => 'Mworkareasub was successfully created.' }
        format.json { render :json => @mworkareasub, :status => :created, :location => @mworkareasub }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mworkareasub.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mworkareasubs/1
  # PUT /mworkareasubs/1.json
  def update
    @mworkareasub = Mworkareasub.find(params[:id])

    respond_to do |format|
      if @mworkareasub.update_attributes(params[:mworkareasub])
        format.html { redirect_to @mworkareasub, :notice => 'Mworkareasub was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mworkareasub.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mworkareasubs/1
  # DELETE /mworkareasubs/1.json
  def destroy
    @mworkareasub = Mworkareasub.find(params[:id])
    @mworkareasub.destroy

    respond_to do |format|
      format.html { redirect_to mworkareasubs_url }
      format.json { head :no_content }
    end
  end
end
