# -*- coding: utf-8 -*-
class TfilebytopicsController < ApplicationController
  include ApplicationHelper
  
  #マスタテキスト用配列
  TOPIC_TYPES = [[0,'アジコム作品の掲載'], [1,'その他の情報の掲載']]
  TOPIC_CATEGORIES = [[0,'家庭用'], [1,'業務用'], [2,'その他']]
  
  # GET /tfilebytopics
  # GET /tfilebytopics.json
  def index
    #@tfilebytopics = Tfilebytopic.all
    
    #@tfilebytopic = Tfilebytopic.new
    
    # @tfilebytopic.topic_type = 0
    # @tfilebytopic.category = 0
#     
    # @tfilebytopic.upload_date=Time.now.strftime('%Y/%m/%d')
    
    # #タイプ
    # @topic_types = TOPIC_TYPES
# 
    # #カテゴリ
    # @topic_categories = TOPIC_CATEGORIES

    respond_to do |format|
      format.html # index.html.erb
     # format.json { render :json => @tfilebytopics }
     # format.json { render json: @tfilebytopics.map{|upload| upload.to_jq_upload } }
    end
  end

  # GET /tfilebytopics/1
  # GET /tfilebytopics/1.json
  def show
    @tfilebytopic = Tfilebytopic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @tfilebytopic }
    end
  end

  # GET /tfilebytopics/new
  # GET /tfilebytopics/new.json
  def new
    @tfilebytopic = Tfilebytopic.new
    @tfilebytopic.ttopic_id = params[:ttopic_id]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @tfilebytopic }
    end
  end

  # GET /tfilebytopics/1/edit
  def edit
    @tfilebytopic = Tfilebytopic.find(params[:id])
  end

    
  # POST /tfilebytopics
  # POST /tfilebytopics.json
  def create
    @tfilebytopic = Tfilebytopic.new(params[:tfilebytopic])

    @tfilebytopic.upload_file = params[:tfilebytopic][:upload_file]
      
      filename = @tfilebytopic.upload_file.file.filename
      content_type = @tfilebytopic.upload_file.file.content_type
      size =  @tfilebytopic.upload_file.file.size
      
      if !isImage(content_type)
        #画像以外の場合
        @tfilebytopic.upload_file_no_image = @tfilebytopic.upload_file
        @tfilebytopic.upload_file = nil
      end
      
      @tfilebytopic.upload_file_name = filename
 
      #トランザクション開始
      begin
        ActiveRecord::Base::transaction() do
  
          respond_to do |format|    
            #format.html { redirect_to new_tfilebytopic_path, :notice => 'アップロードしています。しばらくお待ちください。' }
            format.html { render :action => "new", :notice => 'アップロードしています。しばらくお待ちください。' }
            #format.html { render status: :accepted , :notice => 'アップロードしています。しばらくお待ちください。'}
            env["rack_after_reply.callbacks"] << lambda {
            if @tfilebytopic.save
              #ファイル情報を保存しておく
              if isImage(content_type)
                @tfilebytopic.upload_file_url = @tfilebytopic.upload_file.url
                
                @tfilebytopic.thumbnail_url = @tfilebytopic.upload_file.thumb.url
                @tfilebytopic.thumbnail2_url = @tfilebytopic.upload_file.thumb2.url
              else
                @tfilebytopic.upload_file_url = @tfilebytopic.upload_file_no_image.url
              end
 
              @tfilebytopic.content_type = content_type
              @tfilebytopic.upload_file_size = size
              
              @tfilebytopic.save
              
              format.html { redirect_to "/ttopics/#{@tfilebytopic.ttopic_id}", :notice => '作品、情報をアップロードしました。(大容量ファイルの場合、反映までに時間がかかります。)' }
            end
            }
          end
          
        end #commit

      rescue => exc
        # 例外が発生したときの処理
        logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
        logger.fatal exc
        logger.fatal exc.message
        logger.fatal exc.backtrace.join("\n")
        
      end
  end

  # PUT /tfilebytopics/1
  # PUT /tfilebytopics/1.json
  def update
    @tfilebytopic = Tfilebytopic.find(params[:id])

    respond_to do |format|
      if @tfilebytopic.update_attributes(params[:tfilebytopic])
        format.html { redirect_to @tfilebytopic, :notice => 'Tfilebytopic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @tfilebytopic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /tfilebytopics/1
  # DELETE /tfilebytopics/1.json
  def destroy
    @tfilebytopic = Tfilebytopic.find(params[:id])
    @tfilebytopic.destroy

    respond_to do |format|
      format.html { redirect_to tfilebytopics_url }
      format.json { head :no_content }
    end
  end
  
end
