# -*- coding: utf-8 -*-
class MusersController < ApplicationController
  
  #USER_DIVS = [[0,'アジコム営業'], [1,'アジコムCR担当'], [2,'システム管理者']]
  USER_DIVS = [[0,'協力会社ユーザー'], [1,'アジコム営業'], [2,'アジコムCR担当'], [3,'システム管理者']]
  # GET /musers
  # GET /musers.json
  def index
    @musers = Muser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @musers }
    end
  end
  
  # GET /musers
  # GET /musers.json
  def search
    @user_divs = USER_DIVS
    
    #クエリパラメータ
    @muser = Muser.new
    @muser.user_div = 1
    @muser.user_div = params[:muser][:user_div] if params[:muser]

    @musers = Muser.where(:user_div=>@muser.user_div.to_s).order(:id)

    if !params[:muser].blank?
      render
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render :json => @mcompanies }
      end
    end
  end

  # GET /musers/1
  # GET /musers/1.json
  def show
    @muser = Muser.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @muser }
    end
  end

  # GET /musers/new
  # GET /musers/new.json
  def new
    @muser = Muser.new
    
    @muser.mail = User.current_user.email
    
    #アジコム営業登録
    @muser.user_div = "1"

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @muser }
    end
  end
  
  # GET /musers/new
  # GET /musers/new.json
  def ajicom_new
    @muser = Muser.new
    
    #アジコムCR担当登録
    @muser.user_div = "2"

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @muser }
    end
  end

  # GET /musers/1/edit
  def edit
    @muser = Muser.find(params[:id])
    #password
    user = User.where(:email=>@muser.mail).first
    if !user.blank?
      @muser.password = user.password
    end
  end

  # POST /musers
  # POST /musers.json
  def create
    @muser = Muser.new(params[:muser])
    
    if @muser.user_div == '2'
      #アジコムユーザーの場合はログイン情報も同時に登録する
      @user = User.new
      @user.email = @muser.mail
      @user.password = @muser.password
      #メールを飛ばさない
      @user.confirmed_at = Time.now
      @user.confirmation_sent_at = Time.now
      
      @muser.password = nil

      respond_to do |format|
        begin
          ActiveRecord::Base::transaction() do
            if !@muser.save
              raise
            end
            if !@user.save
              raise
            end
          end
          format.html { redirect_to search_musers_path }
          format.json { render :json => @muser, :status => :created, :location => @muser }
        rescue => exc
          # 例外が発生したときの処理
          logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
          logger.fatal exc
          logger.fatal exc.message
          logger.fatal exc.backtrace.join("\n")

          format.html { render :action => "ajicom_new" }
          if @user.errors
            format.json { render :json => @user.errors, :status => :unprocessable_entity }
          else
            format.json { render :json => @muser.errors, :status => :unprocessable_entity }
          end
        end
      end

    else
      respond_to do |format|
        if @muser.save
          #format.html { redirect_to new_mcompany_path }
          #format.json { render :json => @muser, :status => :created, :location => @muser }
          format.html { redirect_to whatsnew_ttopics_path }
          format.json { head :no_content }
        else
          format.html { render :action => "new" }
          format.json { render :json => @muser.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # PUT /musers/1
  # PUT /musers/1.json
  def update
    @muser = Muser.find(params[:id])

    @user = User.where(:email=>@muser.mail).first
    
    if @user.blank?
      @user = User.new  
      @user.confirmed_at = Time.now
    end
    
    @user.email = params[:muser][:mail]
    @user.password = params[:muser][:password]
    
    if !@user.save
      logger.debug @user.errors.full_messages
    end
    
    params[:muser][:password] = nil

    respond_to do |format|
      if @muser.update_attributes(params[:muser])
        format.html { redirect_to search_musers_path }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @muser.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /musers/1
  # DELETE /musers/1.json
  def destroy
    @muser = Muser.find(params[:id])
    @user = User.where(:email=>@muser[:mail]).first
    
    begin
      ActiveRecord::Base::transaction() do
        mcompany = Mcompany.where(:muser_id=>@muser.id).first
        if !mcompany.blank?
          ttopics = Ttopic.where(:mcompany_id=>mcompany.id)
          ttopic_ids = []
          
          ttopics.each do |ttopic|
            ttopic_ids.push ttopic.id
          end
          
          #投稿作品削除
          if ttopic_ids.count > 0
            Tfilebytopic.delete_all(['ttopic_id in (?)',ttopic_ids])
            Ttopic.destroy_all(:mcompany_id=>mcompany.id)
          end
  
          #会社情報削除
          mcompany.destroy
        end
        
        #ユーザー情報削除
        @muser.destroy
        @user.destroy if @user

      end
    rescue => exc
      # 例外が発生したときの処理
      logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
      logger.fatal exc
      logger.fatal exc.message
      logger.fatal exc.backtrace.join("\n")
    end

    respond_to do |format|
      format.html { redirect_to search_musers_url }
      format.json { head :no_content }
    end
  end
end
