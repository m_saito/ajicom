# -*- coding: utf-8 -*-
class McompaniesController < ApplicationController
  
  require "rexml/document" 
  
  TOPIC_TYPES = [[0,'アジコム作品の掲載'], [1,'その他の情報の掲載']]
  DIVISIONS = [[0,'法人'], [1,'個人']]
  
  # GET /mcompanies
  # GET /mcompanies.json
  def index
    @mcompanies = Mcompany.find(:all, :order => "company_type ASC, id ASC")
    
    @mcompanies.each do |mcompany|
      #改行置き換え
      mcompany.address = mcompany.address.gsub '\r',"\n" if !mcompany.address.blank?
      mcompany.remarks = mcompany.remarks.gsub '\r',"\n" if !mcompany.remarks.blank?
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mcompanies }
    end
  end
  
  # GET /mcompanies
  # GET /mcompanies.json
  def search    
    
    @mcompanies = Mcompany.where('company_name is not null')
    
    @twork_hash={}
    @tworksub_hash={}
    @texpert_hash={}
    
    @mcompany ||= Mcompany.new

    t = Mcompany.arel_table
    
    @params = params
    
    if !@params[:mcompany].blank?
      if @params[:mcompany][:historyback] == "1"
        @params = session[:q]
      else
        session[:q] = @params
      end
      
      company_name = @params[:mcompany][:company_name]
      @mcompany[:company_name]=company_name
      
      if !company_name.blank?
        @mcompanies = @mcompanies.where(t[:company_name].matches("%#{company_name}%").or(t[:company_kana].matches("%#{company_name}%")))
      end
      
      mcompany_ids = []
      
      #得意分野で絞り込み
      if !@params[:texpertises].blank?
        texpertises = []
        @params[:texpertises].each do |k,v|
          if v == '1'
            texpertises.push k
            @texpert_hash[k.to_i]=v
          end
        end
        if texpertises.count > 0
          texpertises = Texpertbycomp.where(["mexpertise_id in (?)", texpertises])
          texpertises.each do |texpertise|
            mcompany_ids.push texpertise.mcompany_id
          end
        end
      end

      # #作業領域で絞り込み
      # if !@params[:tworkareas].blank?
      #   tworkareas = []
      #   @params[:tworkareas].each do |k,v|
      #     if v == '1'
      #       tworkareas.push k
      #       @twork_hash[k.to_i]=v
      #     end
      #   end
        
      #   if tworkareas.count > 0
      #     @tworkareas = Tworkbycomp.where(["mworkarea_id in (?)", tworkareas])
     
      #     @tworkareas.each do |tworkarea|
      #       mcompany_ids.push tworkarea.mcompany_id
      #     end
      #   end
      # end
      
      # if !@params[:tworkareasubs].blank?
      #   tworkareasubs = []
      #   @params[:tworkareasubs].each do |k,v|
      #     if v == '1'
      #       tworkareasubs.push k
      #       @tworksub_hash[k.to_i]=v
      #     end
      #   end
        
      #   if tworkareasubs.count > 0
      #     @tworkareasubs = Tworkbycomp.where(["mworkareasub_id in (?)", tworkareasubs])
     
      #     @tworkareasubs.each do |tworkareasub|
      #       mcompany_ids.push tworkareasub.mcompany_id
      #     end
      #   end
      # end
      
      if mcompany_ids.count > 0
        @mcompanies = @mcompanies.where(["id in (?)", mcompany_ids])
      end
  
      @mcompanies.each do |mcompany|
        #改行置き換え
        mcompany.address = mcompany.address.gsub '\r',"\n" if !mcompany.address.blank?
        mcompany.remarks = mcompany.remarks.gsub '\r',"\n" if !mcompany.remarks.blank?
      end
    else
      #デフォルト検索しない
      @mcompanies=[]
    end
    
    #作業分類マスタ
    @mworkgroups = Mworkgroup.order(:id)
    
    #得意分野マスタ
    @mexpertises = Mexpertise.order(:id)
    
    #クラスセット
    @mworkgroups.each do |mworkgroup|
      mworkgroup.mworkareas.each do |mworkarea|
        if mworkarea.mworkareasubs.count > 0
          mworkarea.mworkareasubs.each do |mworkareasub|
            if mworkgroup.id == 1
              mworkareasub[:class_string]="btn-design "
            elsif mworkgroup.id == 2
              mworkareasub[:class_string]='btn-planning '
            else
              mworkareasub[:class_string]='btn-others '
            end
            
            if @tworksub_hash[mworkareasub.id]=='1'
              mworkareasub[:class_string] += ' active '
              mworkareasub[:btn_active] = "1"
            else
              mworkareasub[:btn_active] = "0"
            end
            
          end
        else
          if mworkgroup.id == 1
            mworkarea[:class_string]='btn-design '
          elsif mworkgroup.id == 2
            mworkarea[:class_string]='btn-planning '
          else
            mworkarea[:class_string]='btn-others '
          end
          
          if @twork_hash[mworkarea.id]=='1'
            mworkarea[:class_string] += ' active '
            mworkarea[:btn_active] = "1"
          else
            mworkarea[:btn_active] = "0"
          end
          
        end
      end
    end
    
    @mexpertises.each do |mexpertise|
      
      mexpertise[:class_string]='btn-genre'
      
      if @texpert_hash[mexpertise.id] == '1'
        mexpertise[:class_string] += ' active '
        mexpertise[:btn_active] = "1"
      else
        mexpertise[:btn_active] = "0"
      end
    end

    if !@params[:mcompany].blank?
      render
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render :json => @mcompanies }
      end
    end
  end

  # GET /mcompanies/1
  # GET /mcompanies/1.json
  def show
    @mcompany = Mcompany.find(params[:id])
    
    #改行置き換え
    @mcompany.address = @mcompany.address.gsub '\r',"\n" if !@mcompany.address.blank?
    @mcompany.remarks = @mcompany.remarks.gsub '\r',"\n" if !@mcompany.remarks.blank?
    
    #作品
    @ttopics = Ttopic.where(:mcompany_id => params[:id]).paginate(:page => params[:page], :order => 'updated_at desc', :per_page => 10)

    @ttopics.each do |ttopic|
      #タイプ
      if ![ttopic.topic_type.to_i].blank?
        ttopic.topic_type = TOPIC_TYPES[ttopic.topic_type.to_i].last
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mcompany }
    end
  end

  # GET /mcompanies/new
  # GET /mcompanies/new.json
  def new
    @mcompany = Mcompany.new
   
    cm_new(@mcompany)
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mcompany }
    end
  end

  def cm_new(mcompany) 
    muser=Muser.where(:mail=>User.current_user.email).first
    @mcompany.muser_id = muser.id
    
    #作業分類マスタ
    @mworkgroups = Mworkgroup.order(:id)
    
    #得意分野マスタ
    @mexpertises = Mexpertise.order(:id)
    
    #個人/法人
    @divisions = DIVISIONS
    
    @twork_hash = {}
    @tworksub_hash = {}
    
    @texpert_hash = {}

    #クラスセット
    @mworkgroups.each do |mworkgroup|
      mworkgroup.mworkareas.each do |mworkarea|
        if mworkarea.mworkareasubs.count > 0
          mworkarea.mworkareasubs.each do |mworkareasub|
            if mworkgroup.id == 1
              mworkareasub[:class_string]="btn-design "
            elsif mworkgroup.id == 2
              mworkareasub[:class_string]='btn-planning '
            else
              mworkareasub[:class_string]='btn-others '
            end
          end
        else
          if mworkgroup.id == 1
            mworkarea[:class_string]='btn-design '
          elsif mworkgroup.id == 2
            mworkarea[:class_string]='btn-planning '
          else
            mworkarea[:class_string]='btn-others '
          end
        end
      end
    end
  end

  # GET /mcompanies/1/edit
  def edit
    @mcompany = Mcompany.find(params[:id])
    #作業分類マスタ
    @mworkgroups = Mworkgroup.order(:id)
    
    #得意分野マスタ
    @mexpertises = Mexpertise.order(:id)
    
    #アジコムユーザー
    @musers = Muser.where(:user_div=>'2').order(:user_kana)
    
    #個人/法人
    @divisions = DIVISIONS

    #作業分類
    @twork_hash = {}
    @tworksub_hash = {}
    @mcompany.tworkbycomps.each do |twork|
      if twork.mworkarea_id
        @twork_hash[twork.mworkarea_id] = '1'
      else
        @tworksub_hash[twork.mworkareasub_id] = '1'
      end
    end
    #得意分野
    @texpert_hash = {}
    @mcompany.texpertbycomps.each do |texpert|
      @texpert_hash[texpert.mexpertise_id] = '1'
    end   

    #設立
    @mcompany.establish_date=@mcompany.establish_date.strftime('%Y/%m/%d') if !@mcompany.establish_date.blank?
    
    #備考
    @mcompany.remarks = @mcompany.remarks.gsub '\r',"\n" if !@mcompany.remarks.blank?
    
    #クラスセット
    @mworkgroups.each do |mworkgroup|
      mworkgroup.mworkareas.each do |mworkarea|
        if mworkarea.mworkareasubs.count > 0
          mworkarea.mworkareasubs.each do |mworkareasub|
            if mworkgroup.id == 1
              mworkareasub[:class_string]="btn-design "
            elsif mworkgroup.id == 2
              mworkareasub[:class_string]='btn-planning '
            else
              mworkareasub[:class_string]='btn-others '
            end
            
            if @tworksub_hash[mworkareasub.id]=='1'
              mworkareasub[:class_string] += ' active '
            end
            
          end
        else
          if mworkgroup.id == 1
            mworkarea[:class_string]='btn-design '
          elsif mworkgroup.id == 2
            mworkarea[:class_string]='btn-planning '
          else
            mworkarea[:class_string]='btn-others '
          end
          
          if @twork_hash[mworkarea.id]=='1'
            mworkarea[:class_string] += ' active '
          end
          
        end
      end
    end
  end
  
  
  
  #協力会社ユーザー追加
  def add_staff   
    render
  end

  # POST /mcompanies
  # POST /mcompanies.json
  def create
    @mcompany = Mcompany.new(params[:mcompany])

    begin
      ActiveRecord::Base::transaction() do
        cm_create(@mcompany)
        respond_to do |format|
          format.html { redirect_to ttopics_path , :notice => '登録しました。' }
          format.json { head :no_content }
        end
      end #commit

    rescue => exc
      # 例外が発生したときの処理
      logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
      logger.fatal exc
      logger.fatal exc.message
      logger.fatal exc.backtrace.join("\n")
      
      respond_to do |format|
        self.new
        format.html { render :action => "new" }
        format.json { render :json => @mcompany.errors, :status => :unprocessable_entity }
      end
    end
  end

  # GET /mcompanies/popup
  # GET /mcompanies/popup.json
  def popup
    if !params[:mcompany].blank?
      @mcompany = Mcompany.new(params[:mcompany])
      # 既に登録している場合はチェックしない
      @chk = Mcompany.where(:company_name=>params[:mcompany][:company_name])
      if !@chk.blank?
        render
        return
      end

      begin
        ActiveRecord::Base::transaction() do
          cm_create(@mcompany)
          render
        end #commit
      rescue => exc
        # 例外が発生したときの処理
        logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
        logger.fatal exc
        logger.fatal exc.message
        logger.fatal exc.backtrace.join("\n")
        
        render
      end
    else
      @mcompany = Mcompany.new
      @mcompany.company_type = params[:type]
      cm_new(@mcompany)
      render :layout => 'popup'
    end
  end

  def cm_create(mcompany)
     if !@mcompany.save
       raise 'Save Fault'
     end

    # #作業分類
    # tworkareas = params[:tworkareas]
    # tworkareas.each do |k,v|
    #   if v == '1'
    #     tworkbycomp = Tworkbycomp.new
    #     tworkbycomp[:mcompany_id] = @mcompany.id
    #     tworkbycomp[:mworkarea_id] = k
    #     tworkbycomp.save
    #   end
    # end
    # tworkareasubs = params[:tworkareasubs]
    # tworkareasubs.each do |k,v|
    #   if v == '1'
    #     tworkbycomp = Tworkbycomp.new
    #     tworkbycomp[:mcompany_id] = @mcompany.id
    #     tworkbycomp[:mworkareasub_id] = k
    #     tworkbycomp.save
    #   end
    # end

    #得意分野
    texpertises = params[:texpertises]
    if !texpertises.blank?
      texpertises.each do |k,v|
        if v == '1'
          texpertbycomp = Texpertbycomp.new
          texpertbycomp[:mcompany_id] = @mcompany.id
          texpertbycomp[:mexpertise_id] = k
          texpertbycomp.save
        end
      end
    end
    #アジコム担当者
    ajicom_users = params[:ajicom_users]
    if !ajicom_users.blank?
      ajicom_users.each do |user_id|
        ajicom_user = Tuserbycomp.new
        ajicom_user[:mcompany_id] = @mcompany.id
        ajicom_user[:muser_id] = user_id
        ajicom_user.save
      end
    end
    #担当者
    tstaffs = params[:tstaffs]
    if !tstaffs.blank?
      tstaffs.each do |staff|
        tstaff = Tstaffbycomp.new
        tstaff[:name] = staff[:name]
        tstaff[:kana] = staff[:kana]
        tstaff[:mail] = staff[:mail]
        tstaff[:mcompany_id] = @mcompany.id
        tstaff.save
      end
    end
    #メール送信
    #McompanyMailer.regist_mail(@mcompany).deliver
  end

  # PUT /mcompanies/1
  # PUT /mcompanies/1.json
  def update
    @mcompany = Mcompany.find(params[:id])
    
    begin
      ActiveRecord::Base::transaction() do

        # #作業分類
        # Tworkbycomp.destroy_all(:mcompany_id=>@mcompany.id)
        # tworkareas = params[:tworkareas]
        # tworkareas.each do |k,v|
        #   if v == '1'
        #     tworkbycomp = Tworkbycomp.new
        #     tworkbycomp[:mcompany_id] = @mcompany.id
        #     tworkbycomp[:mworkarea_id] = k
        #     tworkbycomp.save
        #   end
        # end
        
        # tworkareasubs = params[:tworkareasubs]
        # tworkareasubs.each do |k,v|
        #   if v == '1'
        #     tworkbycomp = Tworkbycomp.new
        #     tworkbycomp[:mcompany_id] = @mcompany.id
        #     tworkbycomp[:mworkareasub_id] = k
        #     tworkbycomp.save
        #   end
        # end

        #得意分野
        Texpertbycomp.destroy_all(:mcompany_id=>@mcompany.id)
        texpertises = params[:texpertises]
        if !texpertises.blank?
          texpertises.each do |k,v|
            if v == '1'
              texpertbycomp = Texpertbycomp.new
              texpertbycomp[:mcompany_id] = @mcompany.id
              texpertbycomp[:mexpertise_id] = k
              texpertbycomp.save
            end
          end
        end
        
        #アジコム担当者
        Tuserbycomp.destroy_all(:mcompany_id=>@mcompany.id)
        ajicom_users = params[:ajicom_users]
        if !ajicom_users.blank?
          ajicom_users.each do |user_id|
            ajicom_user = Tuserbycomp.new
            ajicom_user[:mcompany_id] = @mcompany.id
            ajicom_user[:muser_id] = user_id
            ajicom_user.save
          end
        end
        
        #担当者
        Tstaffbycomp.destroy_all(:mcompany_id=>@mcompany.id)
        tstaffs = params[:tstaffs]
        if !tstaffs.blank?
          tstaffs.each do |staff|
            tstaff = Tstaffbycomp.new
            tstaff[:name] = staff[:name]
            tstaff[:kana] = staff[:kana]
            tstaff[:mail] = staff[:mail]
            tstaff[:mcompany_id] = @mcompany.id
            tstaff.save
          end
        end
        
        #メール送信
        #McompanyMailer.update_mail(@mcompany).deliver

        respond_to do |format|
          if @mcompany.update_attributes(params[:mcompany])
            format.html { redirect_to @mcompany, :notice => '情報を修正しました。' }
            format.json { head :no_content }
          else
            raise 'Save Fault'
          end
        end
      end #commit

    rescue => exc
      # 例外が発生したときの処理
      logger.fatal '[' + Time.now.strftime("%Y/%m/%d %H:%M:%S") + "]" + "[FATAL]"
      logger.fatal exc
      logger.fatal exc.message
      logger.fatal exc.backtrace.join("\n")
      
      respond_to do |format|
        self.edit
        format.html { render :action => "edit" }
        format.json { render :json => @mcompany.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mcompanies/1
  # DELETE /mcompanies/1.json
  def destroy
    @mcompany = Mcompany.find(params[:id])
    
    ttopics = Ttopic.where(:mcompany_id=>@mcompany.id)
    ttopic_ids = []
    
    ttopics.each do |ttopic|
      ttopic_ids.push ttopic.id
    end
    
    #投稿作品削除
    if ttopic_ids.count > 0
      Tfilebytopic.delete_all(['ttopic_id in (?)',ttopic_ids])
      Ttopic.destroy_all(:mcompany_id=>@mcompany.id)
    end
    
    @mcompany.destroy

    respond_to do |format|
      format.html { redirect_to mcompanies_url }
      format.json { head :no_content }
    end
  end
  
  def get_address_data
    zipcode = params[:zipcode].to_s
    uri = "http://api.aoikujira.com/zip/zip.php?zn=" + zipcode
    doc = REXML::Document.new(open(uri))
    @address = ""
    doc.elements.each('ZIP_result/ADDRESS_value') { |elem|
      elem.elements.each('value') {|e|
        if e.attributes["state"] != nil
          @address = @address + e.attributes["state"]
        end
        if e.attributes["address"] != nil
          @address = @address + e.attributes["address"]
        end
        if e.attributes["city"] != nil
          @address = @address + e.attributes["city"]
        end
      }
    }
    render 
  end
  
  #アジコムユーザー追加
  def add_ajicom_user    
    @params = {}
    #musers = Muser.where(:user_div=>'1').order(:login)
    musers = Muser.where(:user_div=>'2').order('user_kana COLLATE "C"')
    @params[:musers] = musers
    
    render
  end

end
