class TexpertbycompsController < ApplicationController
  # GET /texpertbycomps
  # GET /texpertbycomps.json
  def index
    @texpertbycomps = Texpertbycomp.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @texpertbycomps }
    end
  end

  # GET /texpertbycomps/1
  # GET /texpertbycomps/1.json
  def show
    @texpertbycomp = Texpertbycomp.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @texpertbycomp }
    end
  end

  # GET /texpertbycomps/new
  # GET /texpertbycomps/new.json
  def new
    @texpertbycomp = Texpertbycomp.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @texpertbycomp }
    end
  end

  # GET /texpertbycomps/1/edit
  def edit
    @texpertbycomp = Texpertbycomp.find(params[:id])
  end

  # POST /texpertbycomps
  # POST /texpertbycomps.json
  def create
    @texpertbycomp = Texpertbycomp.new(params[:texpertbycomp])

    respond_to do |format|
      if @texpertbycomp.save
        format.html { redirect_to @texpertbycomp, :notice => 'Texpertbycomp was successfully created.' }
        format.json { render :json => @texpertbycomp, :status => :created, :location => @texpertbycomp }
      else
        format.html { render :action => "new" }
        format.json { render :json => @texpertbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /texpertbycomps/1
  # PUT /texpertbycomps/1.json
  def update
    @texpertbycomp = Texpertbycomp.find(params[:id])

    respond_to do |format|
      if @texpertbycomp.update_attributes(params[:texpertbycomp])
        format.html { redirect_to @texpertbycomp, :notice => 'Texpertbycomp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @texpertbycomp.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /texpertbycomps/1
  # DELETE /texpertbycomps/1.json
  def destroy
    @texpertbycomp = Texpertbycomp.find(params[:id])
    @texpertbycomp.destroy

    respond_to do |format|
      format.html { redirect_to texpertbycomps_url }
      format.json { head :no_content }
    end
  end
end
