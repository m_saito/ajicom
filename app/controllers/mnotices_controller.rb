class MnoticesController < ApplicationController
  # GET /mnotices
  # GET /mnotices.json
  def index
    @mnotices = Mnotice.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mnotices }
    end
  end

  # GET /mnotices/1
  # GET /mnotices/1.json
  def show
    @mnotice = Mnotice.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mnotice }
    end
  end

  # GET /mnotices/new
  # GET /mnotices/new.json
  def new
    @mnotice = Mnotice.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mnotice }
    end
  end

  # GET /mnotices/1/edit
  def edit
    @mnotice = Mnotice.find(params[:id])
  end

  # POST /mnotices
  # POST /mnotices.json
  def create
    @mnotice = Mnotice.new(params[:mnotice])

    respond_to do |format|
      if @mnotice.save
        format.html { redirect_to @mnotice, notice: 'Mnotice was successfully created.' }
        format.json { render json: @mnotice, status: :created, location: @mnotice }
      else
        format.html { render action: "new" }
        format.json { render json: @mnotice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mnotices/1
  # PUT /mnotices/1.json
  def update
    @mnotice = Mnotice.find(params[:id])

    respond_to do |format|
      if @mnotice.update_attributes(params[:mnotice])
        format.html { redirect_to @mnotice, notice: 'Mnotice was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mnotice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mnotices/1
  # DELETE /mnotices/1.json
  def destroy
    @mnotice = Mnotice.find(params[:id])
    @mnotice.destroy

    respond_to do |format|
      format.html { redirect_to mnotices_url }
      format.json { head :no_content }
    end
  end
end
