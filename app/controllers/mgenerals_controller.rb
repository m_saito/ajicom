class MgeneralsController < ApplicationController
  # GET /mgenerals
  # GET /mgenerals.json
  def index
    @mgenerals = Mgeneral.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mgenerals }
    end
  end

  # GET /mgenerals/1
  # GET /mgenerals/1.json
  def show
    @mgeneral = Mgeneral.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mgeneral }
    end
  end

  # GET /mgenerals/new
  # GET /mgenerals/new.json
  def new
    @mgeneral = Mgeneral.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mgeneral }
    end
  end

  # GET /mgenerals/1/edit
  def edit
    @mgeneral = Mgeneral.find(params[:id])
  end

  # POST /mgenerals
  # POST /mgenerals.json
  def create
    @mgeneral = Mgeneral.new(params[:mgeneral])

    respond_to do |format|
      if @mgeneral.save
        format.html { redirect_to @mgeneral, :notice => 'Mgeneral was successfully created.' }
        format.json { render :json => @mgeneral, :status => :created, :location => @mgeneral }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mgeneral.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mgenerals/1
  # PUT /mgenerals/1.json
  def update
    @mgeneral = Mgeneral.find(params[:id])

    respond_to do |format|
      if @mgeneral.update_attributes(params[:mgeneral])
        format.html { redirect_to @mgeneral, :notice => 'Mgeneral was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mgeneral.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mgenerals/1
  # DELETE /mgenerals/1.json
  def destroy
    @mgeneral = Mgeneral.find(params[:id])
    @mgeneral.destroy

    respond_to do |format|
      format.html { redirect_to mgenerals_url }
      format.json { head :no_content }
    end
  end
end
