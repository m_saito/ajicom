class McompusersController < ApplicationController
  # GET /mcompusers
  # GET /mcompusers.json
  def index
    @mcompusers = Mcompuser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @mcompusers }
    end
  end

  # GET /mcompusers/1
  # GET /mcompusers/1.json
  def show
    @mcompuser = Mcompuser.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @mcompuser }
    end
  end

  # GET /mcompusers/new
  # GET /mcompusers/new.json
  def new
    @mcompuser = Mcompuser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @mcompuser }
    end
  end

  # GET /mcompusers/1/edit
  def edit
    @mcompuser = Mcompuser.find(params[:id])
  end

  # POST /mcompusers
  # POST /mcompusers.json
  def create
    @mcompuser = Mcompuser.new(params[:mcompuser])

    respond_to do |format|
      if @mcompuser.save
        format.html { redirect_to @mcompuser, :notice => 'Mcompuser was successfully created.' }
        format.json { render :json => @mcompuser, :status => :created, :location => @mcompuser }
      else
        format.html { render :action => "new" }
        format.json { render :json => @mcompuser.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mcompusers/1
  # PUT /mcompusers/1.json
  def update
    @mcompuser = Mcompuser.find(params[:id])

    respond_to do |format|
      if @mcompuser.update_attributes(params[:mcompuser])
        format.html { redirect_to @mcompuser, :notice => 'Mcompuser was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @mcompuser.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mcompusers/1
  # DELETE /mcompusers/1.json
  def destroy
    @mcompuser = Mcompuser.find(params[:id])
    @mcompuser.destroy

    respond_to do |format|
      format.html { redirect_to mcompusers_url }
      format.json { head :no_content }
    end
  end
end
