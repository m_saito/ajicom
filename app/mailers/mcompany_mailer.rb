# -*- coding: utf-8 -*-
class McompanyMailer < ActionMailer::Base
  default from: "info@ajicom.jp"
  
   RECIPIENT = "m_saito@subakolab.com"
#  RECIPIENT = "kimura@subakolab.com"
#  RECIPIENT = "kiyama@subakolab.com"
  
  def regist_mail mcompany
    #全アジコムユーザーへ送信(メーリングリスト)
    contact = RECIPIENT
    #協力会社情報
    @mcompany = mcompany
    
    #作業領域
    @mcompany.tworkareas=[]
    @mcompany.tworkbycomps.each do |twork|
      if twork.mworkarea
        @mcompany.tworkareas.push twork.mworkarea.name
      else
        @mcompany.tworkareas.push twork.mworkareasub.name
      end
    end
    
    #得意分野
    @mcompany.texpertises=[]
    @mcompany.texpertbycomps.each do |texpert|
      @mcompany.texpertises.push texpert.mexpertise.name if texpert.mexpertise
    end
    
    mail(:to => contact,
         :subject => "AJICOM CREATIVE ARCHIVE へ新規会社の登録がありました")
  end
  
  def update_mail mcompany
    #全アジコムユーザーへ送信(メーリングリスト)
    contact = RECIPIENT
    #協力会社情報
    @mcompany = mcompany
    
    #作業領域
    @mcompany.tworkareas=[]
    @mcompany.tworkbycomps.each do |twork|
      if twork.mworkarea
        @mcompany.tworkareas.push twork.mworkarea.name
      else
        @mcompany.tworkareas.push twork.mworkareasub.name
      end
    end
    
    #得意分野
    @mcompany.texpertises=[]
    @mcompany.texpertbycomps.each do |texpert|
      @mcompany.texpertises.push texpert.mexpertise.name if texpert.mexpertise
    end
    
    mail(:to => contact,
         :subject => "AJICOM CREATIVE ARCHIVE で協力会社情報の更新がありました")
  end
end
