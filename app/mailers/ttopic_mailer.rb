# -*- coding: utf-8 -*-
class TtopicMailer < ActionMailer::Base
  default from: "info@ajicom.jp"
  
#  RECIPIENT = "kiyama@subakolab.com"
#  RECIPIENT = "kimura@subakolab.com"
  RECIPIENT = "m_saito@subakolab.com"
  
  TOPIC_TYPES = {'0'=>'アジコム作品の掲載', '1'=>'その他の情報の掲載'}
  TOPIC_CATEGORIES = {'0'=>'家庭用', '1'=>'業務用', '2'=>'その他'}
  
  def regist_mail ttopic
    #全アジコムユーザーへ送信(メーリングリスト)
    contact = RECIPIENT
    #トピック情報
    @ttopic = ttopic
    
    #タイプ
    @ttopic.topic_type = TOPIC_TYPES[@ttopic.topic_type]
    
    #カテゴリ
    @ttopic.category = TOPIC_CATEGORIES[@ttopic.category]
    
    mail(:to => contact,
         :subject => "AJICOM CREATIVE ARCHIVE へ新規投稿がありました")
  end
  
end
