AjicomHtml::Application.routes.draw do
    
  resources :mtopicuses

  resources :mcompanytypes

  resources :mnotices

  resources :tcheckbytopics

  resources :global_searches do
    collection do
      get 'search'
      post 'search'
    end
  end

  resources :tstaffbycomps

  resources :mcategories

  get "welcome/index"

  resources :roles

  root :to => "welcome#index"
  
  devise_for :user
  
  devise_for :users, :only => [:session] do
    get '/sign_in', :to => 'devise/sessions#new', :as => :new_user_session
    get '/sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session
    get '/forgot_password', :to => 'devise/passwords#new', :as => :new_password_session
  end
  
  resources :users do
    get :new
    post :create
    post :destroy
  end
  
  resources :vdivbycomps

  resources :tworkbycomps

  resources :tuserbycomps

  resources :tfilebytopics do
    collection do
      get  'upload'
      post 'upload'
    end
  end

  resources :texpertbycomps

  resources :tcategorybytopics

  resources :mworkgroups

  resources :mworkareasubs

  resources :mworkareas

  resources :musers do
    collection do
      get 'search'
      get 'ajicom_new'
      post 'search'
    end
  end

  resources :mgenerals

  resources :mexpertises

  resources :mcompusers

  resources :mcompanies do
    member do
      get 'order_by_company_name'
      
    end

    collection do
      get 'order_by_company_name'
      get 'search'
      get 'add_ajicom_user'
      get 'add_staff'
      get 'get_address_data'
      get 'popup'
      post 'popup'
      post 'search'
    end
  end
  
  resources :contents
  
  match '/ttopics/client_select'
  match '/ttopics/cmbbrand'
  match '/ttopics/cmbclient_id'
  match '/ttopics/cmbmcompany_id'
  match '/ttopics/cmbmprinting_company'
  resources :ttopics do
    collection do
      get 'history'
      get 'whatsnew'
      get 'search'
      get 'client_select'
      post 'search'
    end
  end

  resources :mbrands do
    collection do
      get 'popup'
      post 'popup'
    end
  end
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
