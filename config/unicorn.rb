rails_root = `pwd`.chomp

if ENV["RAILS_ENV"] == "development"
  worker_processes 5
else
  worker_processes 5
end
listen 3000
pid "#{ rails_root }/tmp/pids/unicorn.pid"
timeout 600
preload_app true

stdout_path "log/unicorn-out.log"
stderr_path "log/unicorn-err.log"

before_fork do |server, worker|
  ActiveRecord::Base.connection.disconnect!
  old_pid = "#{ server.config[:pid] }.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |server, worker|
  ActiveRecord::Base.establish_connection

  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end
end
