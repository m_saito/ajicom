# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "rails-bootstrap-toggle-buttons"
  s.version = "0.0.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Carlos Alexandro Becker"]
  s.date = "2012-10-03"
  s.description = "Just providing the bootstrap-toggle-buttons from https://github.com/nostalgiaz/bootstrap-toggle-buttons into a gem."
  s.email = ["caarlos0@gmail.com"]
  s.homepage = "https://github.com/caarlos0/rails-bootstrap-toggle-buttons.git"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "A simple gem for https://github.com/nostalgiaz/bootstrap-toggle-buttons"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
