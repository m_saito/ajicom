# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{less-rails-bootstrap}
  s.version = "2.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Ken Collins"]
  s.date = %q{2012-10-31}
  s.description = %q{CSS toolkit from Twitter For Rails 3.1 Asset Pipeline. Best one of all!}
  s.email = ["ken@metaskills.net"]
  s.files = [".gitignore", ".travis.yml", "CHANGELOG.md", "Gemfile", "Guardfile", "README.md", "Rakefile", "less-rails-bootstrap.gemspec", "lib/less-rails-bootstrap.rb", "lib/less/rails/bootstrap.rb", "lib/less/rails/bootstrap/engine.rb", "lib/less/rails/bootstrap/version.rb", "scripts/update_bootstrap.sh", "test/cases/engine_spec.rb", "test/cases/usage_css_spec.rb", "test/cases/usage_js_spec.rb", "test/dummy_app/app/assets/javascripts/application.js", "test/dummy_app/app/assets/javascripts/individual.js", "test/dummy_app/app/assets/stylesheets/application.css", "test/dummy_app/app/assets/stylesheets/framework.css.less", "test/dummy_app/app/assets/stylesheets/individual.css.less", "test/dummy_app/init.rb", "test/spec_helper.rb", "vendor/assets/images/twitter/bootstrap/glyphicons-halflings-white.png", "vendor/assets/images/twitter/bootstrap/glyphicons-halflings.png", "vendor/assets/javascripts/twitter/bootstrap.js", "vendor/assets/javascripts/twitter/bootstrap/affix.js", "vendor/assets/javascripts/twitter/bootstrap/alert.js", "vendor/assets/javascripts/twitter/bootstrap/button.js", "vendor/assets/javascripts/twitter/bootstrap/carousel.js", "vendor/assets/javascripts/twitter/bootstrap/collapse.js", "vendor/assets/javascripts/twitter/bootstrap/dropdown.js", "vendor/assets/javascripts/twitter/bootstrap/modal.js", "vendor/assets/javascripts/twitter/bootstrap/popover.js", "vendor/assets/javascripts/twitter/bootstrap/scrollspy.js", "vendor/assets/javascripts/twitter/bootstrap/tab.js", "vendor/assets/javascripts/twitter/bootstrap/tooltip.js", "vendor/assets/javascripts/twitter/bootstrap/transition.js", "vendor/assets/javascripts/twitter/bootstrap/typeahead.js", "vendor/assets/stylesheets/twitter/bootstrap-responsive.css.less", "vendor/assets/stylesheets/twitter/bootstrap.css.less", "vendor/frameworks/twitter/bootstrap.less", "vendor/frameworks/twitter/bootstrap/accordion.less", "vendor/frameworks/twitter/bootstrap/alerts.less", "vendor/frameworks/twitter/bootstrap/bootstrap.less", "vendor/frameworks/twitter/bootstrap/breadcrumbs.less", "vendor/frameworks/twitter/bootstrap/button-groups.less", "vendor/frameworks/twitter/bootstrap/buttons.less", "vendor/frameworks/twitter/bootstrap/carousel.less", "vendor/frameworks/twitter/bootstrap/close.less", "vendor/frameworks/twitter/bootstrap/code.less", "vendor/frameworks/twitter/bootstrap/component-animations.less", "vendor/frameworks/twitter/bootstrap/dropdowns.less", "vendor/frameworks/twitter/bootstrap/forms.less", "vendor/frameworks/twitter/bootstrap/grid.less", "vendor/frameworks/twitter/bootstrap/hero-unit.less", "vendor/frameworks/twitter/bootstrap/labels-badges.less", "vendor/frameworks/twitter/bootstrap/layouts.less", "vendor/frameworks/twitter/bootstrap/media.less", "vendor/frameworks/twitter/bootstrap/mixins.less", "vendor/frameworks/twitter/bootstrap/modals.less", "vendor/frameworks/twitter/bootstrap/navbar.less", "vendor/frameworks/twitter/bootstrap/navs.less", "vendor/frameworks/twitter/bootstrap/pager.less", "vendor/frameworks/twitter/bootstrap/pagination.less", "vendor/frameworks/twitter/bootstrap/popovers.less", "vendor/frameworks/twitter/bootstrap/progress-bars.less", "vendor/frameworks/twitter/bootstrap/reset.less", "vendor/frameworks/twitter/bootstrap/responsive-1200px-min.less", "vendor/frameworks/twitter/bootstrap/responsive-767px-max.less", "vendor/frameworks/twitter/bootstrap/responsive-768px-979px.less", "vendor/frameworks/twitter/bootstrap/responsive-navbar.less", "vendor/frameworks/twitter/bootstrap/responsive-utilities.less", "vendor/frameworks/twitter/bootstrap/responsive.less", "vendor/frameworks/twitter/bootstrap/scaffolding.less", "vendor/frameworks/twitter/bootstrap/sprites.less", "vendor/frameworks/twitter/bootstrap/tables.less", "vendor/frameworks/twitter/bootstrap/thumbnails.less", "vendor/frameworks/twitter/bootstrap/tooltip.less", "vendor/frameworks/twitter/bootstrap/type.less", "vendor/frameworks/twitter/bootstrap/utilities.less", "vendor/frameworks/twitter/bootstrap/variables.less", "vendor/frameworks/twitter/bootstrap/wells.less"]
  s.homepage = %q{http://github.com/metaskills/less-rails-bootstrap}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.6}
  s.summary = %q{CSS toolkit from Twitter For Rails 3.1 Asset Pipeline}
  s.test_files = ["test/cases/engine_spec.rb", "test/cases/usage_css_spec.rb", "test/cases/usage_js_spec.rb", "test/dummy_app/app/assets/javascripts/application.js", "test/dummy_app/app/assets/javascripts/individual.js", "test/dummy_app/app/assets/stylesheets/application.css", "test/dummy_app/app/assets/stylesheets/framework.css.less", "test/dummy_app/app/assets/stylesheets/individual.css.less", "test/dummy_app/init.rb", "test/spec_helper.rb"]

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::RubyGemsVersion) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<less-rails>, ["~> 2.2.0"])
      s.add_development_dependency(%q<minitest>, [">= 0"])
      s.add_development_dependency(%q<guard-minitest>, [">= 0"])
      s.add_development_dependency(%q<rails>, ["~> 3.1"])
    else
      s.add_dependency(%q<less-rails>, ["~> 2.2.0"])
      s.add_dependency(%q<minitest>, [">= 0"])
      s.add_dependency(%q<guard-minitest>, [">= 0"])
      s.add_dependency(%q<rails>, ["~> 3.1"])
    end
  else
    s.add_dependency(%q<less-rails>, ["~> 2.2.0"])
    s.add_dependency(%q<minitest>, [">= 0"])
    s.add_dependency(%q<guard-minitest>, [">= 0"])
    s.add_dependency(%q<rails>, ["~> 3.1"])
  end
end
