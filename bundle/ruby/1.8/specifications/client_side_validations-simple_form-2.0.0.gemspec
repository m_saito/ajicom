# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{client_side_validations-simple_form}
  s.version = "2.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Brian Cardarella"]
  s.date = %q{2012-10-09}
  s.description = %q{SimpleForm Plugin for ClientSideValidaitons}
  s.email = ["bcardarella@gmail.com"]
  s.files = ["client_side_validations-simple_form.gemspec", "lib/client_side_validations-simple_form.rb", "lib/client_side_validations/generators/simple_form.rb", "lib/client_side_validations/simple_form.rb", "lib/client_side_validations/simple_form/engine.rb", "lib/client_side_validations/simple_form/form_builder.rb", "lib/client_side_validations/simple_form/version.rb", "vendor/assets/javascripts/rails.validations.simple_form.js"]
  s.homepage = %q{https://github.com/dockyard/client_side_validations-simple_form}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.6}
  s.summary = %q{SimpleForm Plugin for ClientSideValidations}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::RubyGemsVersion) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<client_side_validations>, ["~> 3.2.0"])
      s.add_runtime_dependency(%q<simple_form>, ["~> 2.0.3"])
      s.add_development_dependency(%q<rails>, ["~> 3.2.0"])
      s.add_development_dependency(%q<mocha>, [">= 0"])
      s.add_development_dependency(%q<m>, [">= 0"])
      s.add_development_dependency(%q<sinatra>, ["~> 1.0"])
      s.add_development_dependency(%q<shotgun>, [">= 0"])
      s.add_development_dependency(%q<thin>, [">= 0"])
      s.add_development_dependency(%q<json>, [">= 0"])
      s.add_development_dependency(%q<coffee-script>, [">= 0"])
    else
      s.add_dependency(%q<client_side_validations>, ["~> 3.2.0"])
      s.add_dependency(%q<simple_form>, ["~> 2.0.3"])
      s.add_dependency(%q<rails>, ["~> 3.2.0"])
      s.add_dependency(%q<mocha>, [">= 0"])
      s.add_dependency(%q<m>, [">= 0"])
      s.add_dependency(%q<sinatra>, ["~> 1.0"])
      s.add_dependency(%q<shotgun>, [">= 0"])
      s.add_dependency(%q<thin>, [">= 0"])
      s.add_dependency(%q<json>, [">= 0"])
      s.add_dependency(%q<coffee-script>, [">= 0"])
    end
  else
    s.add_dependency(%q<client_side_validations>, ["~> 3.2.0"])
    s.add_dependency(%q<simple_form>, ["~> 2.0.3"])
    s.add_dependency(%q<rails>, ["~> 3.2.0"])
    s.add_dependency(%q<mocha>, [">= 0"])
    s.add_dependency(%q<m>, [">= 0"])
    s.add_dependency(%q<sinatra>, ["~> 1.0"])
    s.add_dependency(%q<shotgun>, [">= 0"])
    s.add_dependency(%q<thin>, [">= 0"])
    s.add_dependency(%q<json>, [">= 0"])
    s.add_dependency(%q<coffee-script>, [">= 0"])
  end
end
