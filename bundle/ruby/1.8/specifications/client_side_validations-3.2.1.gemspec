# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{client_side_validations}
  s.version = "3.2.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Brian Cardarella"]
  s.date = %q{2012-10-21}
  s.description = %q{Client Side Validations}
  s.email = ["bcardarella@gmail.com"]
  s.files = ["client_side_validations.gemspec", "lib/client_side_validations.rb", "lib/client_side_validations/action_view.rb", "lib/client_side_validations/action_view/form_builder.rb", "lib/client_side_validations/action_view/form_helper.rb", "lib/client_side_validations/action_view/form_tag_helper.rb", "lib/client_side_validations/active_model.rb", "lib/client_side_validations/active_model/acceptance.rb", "lib/client_side_validations/active_model/exclusion.rb", "lib/client_side_validations/active_model/format.rb", "lib/client_side_validations/active_model/inclusion.rb", "lib/client_side_validations/active_model/length.rb", "lib/client_side_validations/active_model/numericality.rb", "lib/client_side_validations/active_model/presence.rb", "lib/client_side_validations/active_record.rb", "lib/client_side_validations/active_record/middleware.rb", "lib/client_side_validations/active_record/uniqueness.rb", "lib/client_side_validations/config.rb", "lib/client_side_validations/core_ext.rb", "lib/client_side_validations/core_ext/range.rb", "lib/client_side_validations/core_ext/regexp.rb", "lib/client_side_validations/engine.rb", "lib/client_side_validations/files.rb", "lib/client_side_validations/generators.rb", "lib/client_side_validations/generators/rails_validations.rb", "lib/client_side_validations/middleware.rb", "lib/client_side_validations/version.rb", "lib/generators/client_side_validations/copy_assets_generator.rb", "lib/generators/client_side_validations/install_generator.rb", "lib/generators/templates/client_side_validations/initializer.rb", "vendor/assets/javascripts/rails.validations.js"]
  s.homepage = %q{https://github.com/bcardarella/client_side_validations}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.6}
  s.summary = %q{Client Side Validations}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::RubyGemsVersion) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rails>, ["~> 3.2.0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
      s.add_development_dependency(%q<mocha>, [">= 0"])
      s.add_development_dependency(%q<m>, [">= 0"])
      s.add_development_dependency(%q<sinatra>, ["~> 1.0"])
      s.add_development_dependency(%q<shotgun>, [">= 0"])
      s.add_development_dependency(%q<thin>, [">= 0"])
      s.add_development_dependency(%q<json>, [">= 0"])
      s.add_development_dependency(%q<coffee-script>, [">= 0"])
      s.add_development_dependency(%q<jquery-rails>, [">= 0"])
    else
      s.add_dependency(%q<rails>, ["~> 3.2.0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
      s.add_dependency(%q<mocha>, [">= 0"])
      s.add_dependency(%q<m>, [">= 0"])
      s.add_dependency(%q<sinatra>, ["~> 1.0"])
      s.add_dependency(%q<shotgun>, [">= 0"])
      s.add_dependency(%q<thin>, [">= 0"])
      s.add_dependency(%q<json>, [">= 0"])
      s.add_dependency(%q<coffee-script>, [">= 0"])
      s.add_dependency(%q<jquery-rails>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, ["~> 3.2.0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
    s.add_dependency(%q<mocha>, [">= 0"])
    s.add_dependency(%q<m>, [">= 0"])
    s.add_dependency(%q<sinatra>, ["~> 1.0"])
    s.add_dependency(%q<shotgun>, [">= 0"])
    s.add_dependency(%q<thin>, [">= 0"])
    s.add_dependency(%q<json>, [">= 0"])
    s.add_dependency(%q<coffee-script>, [">= 0"])
    s.add_dependency(%q<jquery-rails>, [">= 0"])
  end
end
