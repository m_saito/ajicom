# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{bootstrap-datepicker-rails}
  s.version = "0.6.24"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Gonzalo Rodr\303\255guez-Baltan\303\241s D\303\255az"]
  s.date = %q{2012-10-22}
  s.description = %q{A date picker for Twitter Bootstrap}
  s.email = ["siotopo@gmail.com"]
  s.files = [".gitignore", ".rvmrc", "Gemfile", "README.md", "Rakefile", "bootstrap-datepicker-rails.gemspec", "lib/bootstrap-datepicker-rails.rb", "lib/bootstrap-datepicker-rails/engine.rb", "lib/bootstrap-datepicker-rails/railtie.rb", "lib/bootstrap-datepicker-rails/version.rb", "vendor/assets/javascripts/bootstrap-datepicker/core.js", "vendor/assets/javascripts/bootstrap-datepicker/index.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.bg.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.br.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.cs.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.da.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.de.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.es.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.fi.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.fr.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.id.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.is.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.it.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.ja.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.kr.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.lt.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.lv.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.ms.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.nb.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.nl.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.pl.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.pt.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.ru.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.sl.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.sv.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.th.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.tr.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.js", "vendor/assets/javascripts/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.js", "vendor/assets/stylesheets/bootstrap-datepicker.css"]
  s.homepage = %q{https://github.com/Nerian/bootstrap-datepicker-rails}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.6}
  s.summary = %q{A date picker for Twitter Bootstrap}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::RubyGemsVersion) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<railties>, [">= 3.0"])
      s.add_development_dependency(%q<bundler>, [">= 1.0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
    else
      s.add_dependency(%q<railties>, [">= 3.0"])
      s.add_dependency(%q<bundler>, [">= 1.0"])
      s.add_dependency(%q<rake>, [">= 0"])
    end
  else
    s.add_dependency(%q<railties>, [">= 3.0"])
    s.add_dependency(%q<bundler>, [">= 1.0"])
    s.add_dependency(%q<rake>, [">= 0"])
  end
end
