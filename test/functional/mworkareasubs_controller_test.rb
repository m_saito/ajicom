require 'test_helper'

class MworkareasubsControllerTest < ActionController::TestCase
  setup do
    @mworkareasub = mworkareasubs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mworkareasubs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mworkareasub" do
    assert_difference('Mworkareasub.count') do
      post :create, :mworkareasub => {  }
    end

    assert_redirected_to mworkareasub_path(assigns(:mworkareasub))
  end

  test "should show mworkareasub" do
    get :show, :id => @mworkareasub
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mworkareasub
    assert_response :success
  end

  test "should update mworkareasub" do
    put :update, :id => @mworkareasub, :mworkareasub => {  }
    assert_redirected_to mworkareasub_path(assigns(:mworkareasub))
  end

  test "should destroy mworkareasub" do
    assert_difference('Mworkareasub.count', -1) do
      delete :destroy, :id => @mworkareasub
    end

    assert_redirected_to mworkareasubs_path
  end
end
