require 'test_helper'

class McompanytypesControllerTest < ActionController::TestCase
  setup do
    @mcompanytype = mcompanytypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mcompanytypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mcompanytype" do
    assert_difference('Mcompanytype.count') do
      post :create, mcompanytype: { another_name: @mcompanytype.another_name, created_by: @mcompanytype.created_by, id: @mcompanytype.id, name: @mcompanytype.name, updated_by: @mcompanytype.updated_by }
    end

    assert_redirected_to mcompanytype_path(assigns(:mcompanytype))
  end

  test "should show mcompanytype" do
    get :show, id: @mcompanytype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mcompanytype
    assert_response :success
  end

  test "should update mcompanytype" do
    put :update, id: @mcompanytype, mcompanytype: { another_name: @mcompanytype.another_name, created_by: @mcompanytype.created_by, id: @mcompanytype.id, name: @mcompanytype.name, updated_by: @mcompanytype.updated_by }
    assert_redirected_to mcompanytype_path(assigns(:mcompanytype))
  end

  test "should destroy mcompanytype" do
    assert_difference('Mcompanytype.count', -1) do
      delete :destroy, id: @mcompanytype
    end

    assert_redirected_to mcompanytypes_path
  end
end
