require 'test_helper'

class TworkbycompsControllerTest < ActionController::TestCase
  setup do
    @tworkbycomp = tworkbycomps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tworkbycomps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tworkbycomp" do
    assert_difference('Tworkbycomp.count') do
      post :create, :tworkbycomp => {  }
    end

    assert_redirected_to tworkbycomp_path(assigns(:tworkbycomp))
  end

  test "should show tworkbycomp" do
    get :show, :id => @tworkbycomp
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @tworkbycomp
    assert_response :success
  end

  test "should update tworkbycomp" do
    put :update, :id => @tworkbycomp, :tworkbycomp => {  }
    assert_redirected_to tworkbycomp_path(assigns(:tworkbycomp))
  end

  test "should destroy tworkbycomp" do
    assert_difference('Tworkbycomp.count', -1) do
      delete :destroy, :id => @tworkbycomp
    end

    assert_redirected_to tworkbycomps_path
  end
end
