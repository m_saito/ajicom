require 'test_helper'

class MworkgroupsControllerTest < ActionController::TestCase
  setup do
    @mworkgroup = mworkgroups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mworkgroups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mworkgroup" do
    assert_difference('Mworkgroup.count') do
      post :create, :mworkgroup => {  }
    end

    assert_redirected_to mworkgroup_path(assigns(:mworkgroup))
  end

  test "should show mworkgroup" do
    get :show, :id => @mworkgroup
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mworkgroup
    assert_response :success
  end

  test "should update mworkgroup" do
    put :update, :id => @mworkgroup, :mworkgroup => {  }
    assert_redirected_to mworkgroup_path(assigns(:mworkgroup))
  end

  test "should destroy mworkgroup" do
    assert_difference('Mworkgroup.count', -1) do
      delete :destroy, :id => @mworkgroup
    end

    assert_redirected_to mworkgroups_path
  end
end
