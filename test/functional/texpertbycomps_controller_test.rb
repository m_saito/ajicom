require 'test_helper'

class TexpertbycompsControllerTest < ActionController::TestCase
  setup do
    @texpertbycomp = texpertbycomps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:texpertbycomps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create texpertbycomp" do
    assert_difference('Texpertbycomp.count') do
      post :create, :texpertbycomp => {  }
    end

    assert_redirected_to texpertbycomp_path(assigns(:texpertbycomp))
  end

  test "should show texpertbycomp" do
    get :show, :id => @texpertbycomp
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @texpertbycomp
    assert_response :success
  end

  test "should update texpertbycomp" do
    put :update, :id => @texpertbycomp, :texpertbycomp => {  }
    assert_redirected_to texpertbycomp_path(assigns(:texpertbycomp))
  end

  test "should destroy texpertbycomp" do
    assert_difference('Texpertbycomp.count', -1) do
      delete :destroy, :id => @texpertbycomp
    end

    assert_redirected_to texpertbycomps_path
  end
end
