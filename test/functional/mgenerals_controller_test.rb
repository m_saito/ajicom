require 'test_helper'

class MgeneralsControllerTest < ActionController::TestCase
  setup do
    @mgeneral = mgenerals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mgenerals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mgeneral" do
    assert_difference('Mgeneral.count') do
      post :create, :mgeneral => {  }
    end

    assert_redirected_to mgeneral_path(assigns(:mgeneral))
  end

  test "should show mgeneral" do
    get :show, :id => @mgeneral
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mgeneral
    assert_response :success
  end

  test "should update mgeneral" do
    put :update, :id => @mgeneral, :mgeneral => {  }
    assert_redirected_to mgeneral_path(assigns(:mgeneral))
  end

  test "should destroy mgeneral" do
    assert_difference('Mgeneral.count', -1) do
      delete :destroy, :id => @mgeneral
    end

    assert_redirected_to mgenerals_path
  end
end
