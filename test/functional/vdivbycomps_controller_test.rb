require 'test_helper'

class VdivbycompsControllerTest < ActionController::TestCase
  setup do
    @vdivbycomp = vdivbycomps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vdivbycomps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vdivbycomp" do
    assert_difference('Vdivbycomp.count') do
      post :create, :vdivbycomp => {  }
    end

    assert_redirected_to vdivbycomp_path(assigns(:vdivbycomp))
  end

  test "should show vdivbycomp" do
    get :show, :id => @vdivbycomp
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @vdivbycomp
    assert_response :success
  end

  test "should update vdivbycomp" do
    put :update, :id => @vdivbycomp, :vdivbycomp => {  }
    assert_redirected_to vdivbycomp_path(assigns(:vdivbycomp))
  end

  test "should destroy vdivbycomp" do
    assert_difference('Vdivbycomp.count', -1) do
      delete :destroy, :id => @vdivbycomp
    end

    assert_redirected_to vdivbycomps_path
  end
end
