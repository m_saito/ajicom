require 'test_helper'

class McompusersControllerTest < ActionController::TestCase
  setup do
    @mcompuser = mcompusers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mcompusers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mcompuser" do
    assert_difference('Mcompuser.count') do
      post :create, :mcompuser => {  }
    end

    assert_redirected_to mcompuser_path(assigns(:mcompuser))
  end

  test "should show mcompuser" do
    get :show, :id => @mcompuser
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mcompuser
    assert_response :success
  end

  test "should update mcompuser" do
    put :update, :id => @mcompuser, :mcompuser => {  }
    assert_redirected_to mcompuser_path(assigns(:mcompuser))
  end

  test "should destroy mcompuser" do
    assert_difference('Mcompuser.count', -1) do
      delete :destroy, :id => @mcompuser
    end

    assert_redirected_to mcompusers_path
  end
end
