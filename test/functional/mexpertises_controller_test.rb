require 'test_helper'

class MexpertisesControllerTest < ActionController::TestCase
  setup do
    @mexpertise = mexpertises(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mexpertises)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mexpertise" do
    assert_difference('Mexpertise.count') do
      post :create, :mexpertise => {  }
    end

    assert_redirected_to mexpertise_path(assigns(:mexpertise))
  end

  test "should show mexpertise" do
    get :show, :id => @mexpertise
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mexpertise
    assert_response :success
  end

  test "should update mexpertise" do
    put :update, :id => @mexpertise, :mexpertise => {  }
    assert_redirected_to mexpertise_path(assigns(:mexpertise))
  end

  test "should destroy mexpertise" do
    assert_difference('Mexpertise.count', -1) do
      delete :destroy, :id => @mexpertise
    end

    assert_redirected_to mexpertises_path
  end
end
