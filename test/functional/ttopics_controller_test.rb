require 'test_helper'

class TtopicsControllerTest < ActionController::TestCase
  setup do
    @ttopic = ttopics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ttopics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ttopic" do
    assert_difference('Ttopic.count') do
      post :create, :ttopic => {  }
    end

    assert_redirected_to ttopic_path(assigns(:ttopic))
  end

  test "should show ttopic" do
    get :show, :id => @ttopic
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @ttopic
    assert_response :success
  end

  test "should update ttopic" do
    put :update, :id => @ttopic, :ttopic => {  }
    assert_redirected_to ttopic_path(assigns(:ttopic))
  end

  test "should destroy ttopic" do
    assert_difference('Ttopic.count', -1) do
      delete :destroy, :id => @ttopic
    end

    assert_redirected_to ttopics_path
  end
end
