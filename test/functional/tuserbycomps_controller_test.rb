require 'test_helper'

class TuserbycompsControllerTest < ActionController::TestCase
  setup do
    @tuserbycomp = tuserbycomps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tuserbycomps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tuserbycomp" do
    assert_difference('Tuserbycomp.count') do
      post :create, :tuserbycomp => {  }
    end

    assert_redirected_to tuserbycomp_path(assigns(:tuserbycomp))
  end

  test "should show tuserbycomp" do
    get :show, :id => @tuserbycomp
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @tuserbycomp
    assert_response :success
  end

  test "should update tuserbycomp" do
    put :update, :id => @tuserbycomp, :tuserbycomp => {  }
    assert_redirected_to tuserbycomp_path(assigns(:tuserbycomp))
  end

  test "should destroy tuserbycomp" do
    assert_difference('Tuserbycomp.count', -1) do
      delete :destroy, :id => @tuserbycomp
    end

    assert_redirected_to tuserbycomps_path
  end
end
