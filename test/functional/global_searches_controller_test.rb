require 'test_helper'

class GlobalSearchesControllerTest < ActionController::TestCase
  setup do
    @global_search = global_searches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:global_searches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create global_search" do
    assert_difference('GlobalSearch.count') do
      post :create, global_search: {  }
    end

    assert_redirected_to global_search_path(assigns(:global_search))
  end

  test "should show global_search" do
    get :show, id: @global_search
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @global_search
    assert_response :success
  end

  test "should update global_search" do
    put :update, id: @global_search, global_search: {  }
    assert_redirected_to global_search_path(assigns(:global_search))
  end

  test "should destroy global_search" do
    assert_difference('GlobalSearch.count', -1) do
      delete :destroy, id: @global_search
    end

    assert_redirected_to global_searches_path
  end
end
