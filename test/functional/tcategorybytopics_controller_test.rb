require 'test_helper'

class TcategorybytopicsControllerTest < ActionController::TestCase
  setup do
    @tcategorybytopic = tcategorybytopics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tcategorybytopics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tcategorybytopic" do
    assert_difference('Tcategorybytopic.count') do
      post :create, :tcategorybytopic => {  }
    end

    assert_redirected_to tcategorybytopic_path(assigns(:tcategorybytopic))
  end

  test "should show tcategorybytopic" do
    get :show, :id => @tcategorybytopic
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @tcategorybytopic
    assert_response :success
  end

  test "should update tcategorybytopic" do
    put :update, :id => @tcategorybytopic, :tcategorybytopic => {  }
    assert_redirected_to tcategorybytopic_path(assigns(:tcategorybytopic))
  end

  test "should destroy tcategorybytopic" do
    assert_difference('Tcategorybytopic.count', -1) do
      delete :destroy, :id => @tcategorybytopic
    end

    assert_redirected_to tcategorybytopics_path
  end
end
