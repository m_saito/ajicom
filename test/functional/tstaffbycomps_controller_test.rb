require 'test_helper'

class TstaffbycompsControllerTest < ActionController::TestCase
  setup do
    @tstaffbycomp = tstaffbycomps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tstaffbycomps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tstaffbycomp" do
    assert_difference('Tstaffbycomp.count') do
      post :create, tstaffbycomp: { kana: @tstaffbycomp.kana, mail: @tstaffbycomp.mail, mcompany_id: @tstaffbycomp.mcompany_id, name: @tstaffbycomp.name }
    end

    assert_redirected_to tstaffbycomp_path(assigns(:tstaffbycomp))
  end

  test "should show tstaffbycomp" do
    get :show, id: @tstaffbycomp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tstaffbycomp
    assert_response :success
  end

  test "should update tstaffbycomp" do
    put :update, id: @tstaffbycomp, tstaffbycomp: { kana: @tstaffbycomp.kana, mail: @tstaffbycomp.mail, mcompany_id: @tstaffbycomp.mcompany_id, name: @tstaffbycomp.name }
    assert_redirected_to tstaffbycomp_path(assigns(:tstaffbycomp))
  end

  test "should destroy tstaffbycomp" do
    assert_difference('Tstaffbycomp.count', -1) do
      delete :destroy, id: @tstaffbycomp
    end

    assert_redirected_to tstaffbycomps_path
  end
end
