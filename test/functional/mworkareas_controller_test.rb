require 'test_helper'

class MworkareasControllerTest < ActionController::TestCase
  setup do
    @mworkarea = mworkareas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mworkareas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mworkarea" do
    assert_difference('Mworkarea.count') do
      post :create, :mworkarea => {  }
    end

    assert_redirected_to mworkarea_path(assigns(:mworkarea))
  end

  test "should show mworkarea" do
    get :show, :id => @mworkarea
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mworkarea
    assert_response :success
  end

  test "should update mworkarea" do
    put :update, :id => @mworkarea, :mworkarea => {  }
    assert_redirected_to mworkarea_path(assigns(:mworkarea))
  end

  test "should destroy mworkarea" do
    assert_difference('Mworkarea.count', -1) do
      delete :destroy, :id => @mworkarea
    end

    assert_redirected_to mworkareas_path
  end
end
