require 'test_helper'

class TcheckbytopicsControllerTest < ActionController::TestCase
  setup do
    @tcheckbytopic = tcheckbytopics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tcheckbytopics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tcheckbytopic" do
    assert_difference('Tcheckbytopic.count') do
      post :create, tcheckbytopic: { created_by: @tcheckbytopic.created_by, id: @tcheckbytopic.id, mcheck_id: @tcheckbytopic.mcheck_id, name: @tcheckbytopic.name, ttopic_id: @tcheckbytopic.ttopic_id, updated_by: @tcheckbytopic.updated_by }
    end

    assert_redirected_to tcheckbytopic_path(assigns(:tcheckbytopic))
  end

  test "should show tcheckbytopic" do
    get :show, id: @tcheckbytopic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tcheckbytopic
    assert_response :success
  end

  test "should update tcheckbytopic" do
    put :update, id: @tcheckbytopic, tcheckbytopic: { created_by: @tcheckbytopic.created_by, id: @tcheckbytopic.id, mcheck_id: @tcheckbytopic.mcheck_id, name: @tcheckbytopic.name, ttopic_id: @tcheckbytopic.ttopic_id, updated_by: @tcheckbytopic.updated_by }
    assert_redirected_to tcheckbytopic_path(assigns(:tcheckbytopic))
  end

  test "should destroy tcheckbytopic" do
    assert_difference('Tcheckbytopic.count', -1) do
      delete :destroy, id: @tcheckbytopic
    end

    assert_redirected_to tcheckbytopics_path
  end
end
