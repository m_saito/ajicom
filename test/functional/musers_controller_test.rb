require 'test_helper'

class MusersControllerTest < ActionController::TestCase
  setup do
    @muser = musers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:musers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create muser" do
    assert_difference('Muser.count') do
      post :create, :muser => {  }
    end

    assert_redirected_to muser_path(assigns(:muser))
  end

  test "should show muser" do
    get :show, :id => @muser
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @muser
    assert_response :success
  end

  test "should update muser" do
    put :update, :id => @muser, :muser => {  }
    assert_redirected_to muser_path(assigns(:muser))
  end

  test "should destroy muser" do
    assert_difference('Muser.count', -1) do
      delete :destroy, :id => @muser
    end

    assert_redirected_to musers_path
  end
end
