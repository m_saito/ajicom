require 'test_helper'

class MnoticesControllerTest < ActionController::TestCase
  setup do
    @mnotice = mnotices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mnotices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mnotice" do
    assert_difference('Mnotice.count') do
      post :create, mnotice: { another_name: @mnotice.another_name, created_by: @mnotice.created_by, id: @mnotice.id, name: @mnotice.name, updated_by: @mnotice.updated_by }
    end

    assert_redirected_to mnotice_path(assigns(:mnotice))
  end

  test "should show mnotice" do
    get :show, id: @mnotice
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mnotice
    assert_response :success
  end

  test "should update mnotice" do
    put :update, id: @mnotice, mnotice: { another_name: @mnotice.another_name, created_by: @mnotice.created_by, id: @mnotice.id, name: @mnotice.name, updated_by: @mnotice.updated_by }
    assert_redirected_to mnotice_path(assigns(:mnotice))
  end

  test "should destroy mnotice" do
    assert_difference('Mnotice.count', -1) do
      delete :destroy, id: @mnotice
    end

    assert_redirected_to mnotices_path
  end
end
