require 'test_helper'

class McompaniesControllerTest < ActionController::TestCase
  setup do
    @mcompany = mcompanies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mcompanies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mcompany" do
    assert_difference('Mcompany.count') do
      post :create, :mcompany => {  }
    end

    assert_redirected_to mcompany_path(assigns(:mcompany))
  end

  test "should show mcompany" do
    get :show, :id => @mcompany
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @mcompany
    assert_response :success
  end

  test "should update mcompany" do
    put :update, :id => @mcompany, :mcompany => {  }
    assert_redirected_to mcompany_path(assigns(:mcompany))
  end

  test "should destroy mcompany" do
    assert_difference('Mcompany.count', -1) do
      delete :destroy, :id => @mcompany
    end

    assert_redirected_to mcompanies_path
  end
end
