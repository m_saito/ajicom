require 'test_helper'

class TfilebytopicsControllerTest < ActionController::TestCase
  setup do
    @tfilebytopic = tfilebytopics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tfilebytopics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tfilebytopic" do
    assert_difference('Tfilebytopic.count') do
      post :create, :tfilebytopic => {  }
    end

    assert_redirected_to tfilebytopic_path(assigns(:tfilebytopic))
  end

  test "should show tfilebytopic" do
    get :show, :id => @tfilebytopic
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @tfilebytopic
    assert_response :success
  end

  test "should update tfilebytopic" do
    put :update, :id => @tfilebytopic, :tfilebytopic => {  }
    assert_redirected_to tfilebytopic_path(assigns(:tfilebytopic))
  end

  test "should destroy tfilebytopic" do
    assert_difference('Tfilebytopic.count', -1) do
      delete :destroy, :id => @tfilebytopic
    end

    assert_redirected_to tfilebytopics_path
  end
end
